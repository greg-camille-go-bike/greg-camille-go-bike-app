require("dotenv").config({
  path: `.env.${process.env.NODE_ENV}`,
})

module.exports = {
  siteMetadata: {
    siteUrl: "https://gregcamillego.bike",
    title: "Greg & Camille Go Bike",
  },
  plugins: [
    {
      resolve:"gatsby-plugin-sass",
      options: {
        sassOptions: {
          quietDeps: true
        }
      }
    },
    {
      resolve: "gatsby-plugin-netlify-cms",
      options: {
        modulePath: `${__dirname}/src/cms/cms.js`,
        enableIdentityWidget: false,
        manualInit: true
      }
    },
    "gatsby-plugin-image",
    "gatsby-plugin-sharp",
    {
      resolve: "gatsby-plugin-manifest",
      options: {
        icon: "src/images/GClogogrey.png"
      }
    },
    {
      resolve: "gatsby-source-filesystem",
      options: {
        name: `pages`,
        path: `${__dirname}/src/content/pages`,
      }
    },
    {
      resolve: "gatsby-source-filesystem",
      options: {
        name: `sourceMedia`,
        path: `${__dirname}/../assets/source`,
      }
    },
    {
      resolve: "gatsby-source-filesystem",
      options: {
        name: `media`,
        path: `${__dirname}/src/content/media`,
      }
    },
    {
      resolve: "gatsby-source-filesystem",
      options: {
        name: `settings`,
        path: `${__dirname}/src/content/settings`,
      }
    },
    {
      resolve: "gatsby-plugin-mdx",
      options: {
        extensions: [".mdx", ".md"]
      }
    },
    "gatsby-transformer-json",
    "gatsby-transformer-yaml",
    "gatsby-transformer-sharp"
  ],
};
