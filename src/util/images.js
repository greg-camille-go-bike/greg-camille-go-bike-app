export const resizeImageData = (imageData, sizes) => {
    const resizedSources = imageData.images.sources.map(x => { return { ...x, sizes }; });
    return {
        ...imageData,
        images: {
        ...imageData.images,
        fallback: {
            ...imageData.images.fallback,
            sizes
        },
        sources: resizedSources
        }
    }
}

const imageUtils = {
  resizeImageData
}

export default imageUtils;
