export const formatEditLink = (entityName, slug) => {
  return `/admin/#/collections/${entityName}/entries/${slug}`
}

const utils = {
  formatEditLink
}

export default utils;
