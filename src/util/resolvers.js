const moment = require("moment")

const formatDate = (date) => {
  return null != date && date.length > 0 ? moment(date, "YYYY-MM-DD").format("MMMM Do, YYYY") : null;
}

const formatDateLine = (startDate, endDate) => {
  if(endDate) {
    return `${formatDate(startDate)} - ${formatDate(endDate)}`
  }
  else {
    return formatDate(startDate);
  }
}

exports.formatDate = formatDate;
exports.formatDateLine = formatDateLine;
