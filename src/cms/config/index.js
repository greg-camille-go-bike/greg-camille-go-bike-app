import backends from './backends'
import * as Collections from './collections'

const config = {
  ...backends(),
  "load_config_file": false,
  "media_library": { "name": "disabled" },
  "media_folder": "src/images",
  "public_folder": "/images",
  "collections": [
    Collections.pageLinks(),
    Collections.pages(),
    Collections.tours(),
    Collections.journals(),
    Collections.images(),
    Collections.videos(),
    Collections.gpsRecording(),
    Collections.settings()
  ]
}

export default config
