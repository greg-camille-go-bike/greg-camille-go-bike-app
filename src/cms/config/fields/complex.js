import * as Fields from "./simple"
import { randomId, navLabel } from "./simple"

export const mediaSource = () => {
  return {
    "label": "Source",
    "name": "source",
    "widget": "object",
    "collapsed": true,
    "summary": "{{fileName}} ({{width}}x{{height}})",
    "fields": [
      {
        "label": "File",
        "name": "fileName",
        "widget": "string",
        "description": "The name of the file as it is stored/served from the media library"
      },
      {
        "label": "Size",
        "name": "fileSize",
        "widget": "number",
        "value_type": "int",
        "min": 1,
        "description": "The size of the file in bytes"
      },
      {
        "label": "Width",
        "name": "width",
        "widget": "number",
        "value_type": "int",
        "min": 1,
        "description": "Width of the media in pixels"
      },
      {
        "label": "Height",
        "name": "height",
        "widget": "number",
        "value_type": "int",
        "min": 1,
        "height": "Height of the media in pixels"
      }
    ]
  }
}

export const mediaLibraries = () => {
  return {
    "label": "Media Libraries",
    "label_singular": "Media Library",
    "name": "libraries",
    "widget": "list",
    "allow_add": false,
    "fields": [
      Fields.customId(),
      Fields.title()
    ]
  }
}

export const mediaCapture = () => {
  return {
    "label": "Capture",
    "name": "capture",
    "widget": "object",
    "collapsed": true,
    "summary": "{{localDateTime}}",
    "fields": [
      {
        "label": "Date",
        "name": "dateTime",
        "widget": "datetime",
        "format": "YYYY-MM-DDTHH:mm:ss[Z]",
        "picker_utc": true,
        "description": "Date and time the media was captured in UTC timezone"
      },
      {
        "label": "Local Date",
        "name": "localDateTime",
        "widget": "datetime",
        "format": "YYYY-MM-DDTHH:mm:ssZ",
        "picker_utc": false,
        "description": "Date and time the media was captured in the local timezone"
      },
      {
        "label": "Location",
        "name": "location",
        "widget": "map",
        "type": "Point",
        "required": false,
        "description": "Location where the media was captured"
      }
    ]
  }
}

export const navItems = (maxDepth=2) => {

  const items = {
    "label": "Items",
    "label_singular": "Item",
    "name": "items",
    "widget": "list",
    "types": [
      {
        "label": "Link",
        "name": "link",
        "widget": "object",
        "summary": "{{fields.label}}",
        "fields": [
          Fields.randomId(),
          Fields.navLabel(),
          // TODO change to a relation (or maybe there's justification for both internal & external links)
          { "label": "Url", "name": "url", "widget": "string" }
        ]
      },
      {
        "label": "Spacer",
        "name": "spacer",
        "widget": "object",
        "summary": "-----",
        "fields": [
          Fields.randomId()
        ]
      }
    ]
  }

  if(maxDepth > 1) {
    items.types.push({
      "label": "Group",
      "name": "group",
      "widget": "object",
      "summary": "{{fields.label}}",
      "fields": [
        Fields.randomId(),
        Fields.navLabel(),
        navItems(maxDepth - 1)
      ]
    })
  }

  return items;
}

const complex = {
  mediaCapture,
  mediaLibraries,
  mediaSource,
  navItems
}

export default complex;
