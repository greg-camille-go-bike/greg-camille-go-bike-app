export const relatedPage = () => {
  return {
    "label": "Related Page",
    "name": "pageRelated",
    "widget": "relation",
    "collection": "pageLink",
    "search_fields": [ "title" ],
    "value_field": "{{pageSlug}}",
    "display_fields": [ "title", "pageType" ]
  }
}

export const relatedPageParent = () => {
  return {
    ...relatedPage(),
    "label": "Parent Page",
    "name": "pageParent"
  }
}

export const relatedPageParentTour = () => {
  return {
    ...relatedPage(),
    "label": "Parent Tour",
    "name": "pageParent",
    "collection": "tours"
  }
}

export const relatedLibrary = () => {
  return {
    "label": "Library",
    "name": "library", // TODO change to library
    "widget": "relation",
    "collection": "settings",
    "file": "media",
    "search_fields": ["libraries.*.title"],
    "display_fields": ["libraries.*.title"],
    "value_field": "libraries.*.id"
  }
}

export const relatedOwnedAssetLibrary = () => {
  return {
    ...relatedLibrary(),
    "hint": "Default media library for this page and any child pages that don't explicitly override the relationship."
  }
}

export const relatedOwningAssetLibrary = () => {
  return {
    ...relatedLibrary(),
    "hint": "Media library which owns this asset."
  }
}

export const relatedImage = ({label, name, multiple}) => {
  return {
    "label": label,
    "name": name,
    "widget": "relation",
    "collection": "images",
    "required": false,
    "multiple": !! multiple,
    "search_fields": [ "title" ],
    "value_field": "{{title}}",
    "display_fields": [ "title" ]
  }
}

export const relatedFeatureImage = () => {
  return relatedImage({
    "label": "Feature Image",
    "name": "featureImage"
  });
}

export const relatedGalleryImages = () => {
  return relatedImage({
    "label": "Gallery Images",
    "name": "galleryImages",
    "multiple": true
  });
}

export const relatedTags = () => {
  return {
    "name": "tag",
    "label": "Tag",
    "widget": "relation",
    "multiple": true,
    "required": false,
    "collection": "settings",
    "file": "tags",
    "search_fields": ["tags.*.title"],
    "display_fields": ["tags.*.title"],
    "value_field": "tags.*.title"
  }
}

const relations = {
  relatedPage,
  relatedPageParent,
  relatedPageParentTour,
  relatedLibrary,
  relatedImage,
  relatedFeatureImage,
  relatedGalleryImages,
  relatedTags
}

export default relations
