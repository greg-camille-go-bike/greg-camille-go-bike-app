export const entityType = (defaultValue) => {
  if(!defaultValue) {
    throw new Error(`A default value is required for field "entityType"`)
  }

  return {
    "label": "Entity Type",
    "name": "entityType",
    "widget": "hidden",
    "default": defaultValue,
    "description": "A descriminator value used by Netlify CMS and Gatsby to identify the type of resource"
  }
}

export const randomId = () => {
  return {
    "label": "Id",
    "name": "id",
    "widget": "uuid"
  }
}

export const slugId = () => {
  return {
    "label": "Id",
    "name": "id",
    "widget": "hidden",
    "default": "{{slug}}"
  }
}

export const customId = () => {
  return {
    "label": "Id",
    "name": "id",
    "widget": "hidden"
  }
}

export const pageSlug = () => {
  return {
    "label": "Page Slug",
    "name": "pageSlug",
    "widget": "hidden",
    "default": "{{slug}}"
  }
}

export const pageType = (defaultValue) => {
  return {
    "label": "Page Type",
    "name": "pageType",
    "widget": "hidden",
    "default": defaultValue,
    "description": "A descriminator value used by Netlify CMS and Gatsby to identify the type of resource"
  }
}

export const title = () => {
  return {
    "label": "Title",
    "name": "title",
    "widget": "string"
  }
}

const dateCommon = () => {
  return {
    "widget": "datetime",
    "date_format": "MM.DD.YYYY",
    "time_format": false,
    "format": "YYYY-MM-DD",
    "picker_utc": true
  }
}

export const date = () => {
  return {
    "label": "Date",
    "name": "date",
    ...dateCommon()
  };
}

export const dateAuthored = () => {
  return {
    "label": "Date Authored",
    "name": "authoredDate",
    ...dateCommon()
  }
}

export const dateStart = () => {
  return {
    "label": "Start Date",
    "name": "startDate",
    ...dateCommon()
  }
}

export const dateEnd = () => {
  return {
    "label": "End Date",
    "name": "endDate",
    ...dateCommon(),
    "required": false,
    "default": ""
  }
}

export const isDraft = () => {
  return {
    "label": "Draft",
    "name": "draft",
    "widget": "boolean",
    "default": true
  }
}


export const body = () => {
  return {
    "label": "Body",
    "name": "body",
    "widget": "markdown"
  }
}

export const description = () => {
  return {
    "label": "Description",
    "name": "description",
    "widget": "string",
    "description": "An explanation of the content or purpose of this entry"
  }
}

export const mediaDescription = () => {
  return {
    "label": "Description",
    "name": "description",
    "widget": "string",
    "description": "A description of the contents of the media asset.",
    "default": " ",
    "required": false
  }
}

export const mediaScope = () => {
  return {
    "label": "Scope",
    "name": "scope",
    "widget": "select",
    "options": [ "global", "collection", "post", "hidden" ],
    "default": "post",
    "description": "Configures default areas where the media will appear on the website."
  }
}

export const navLabel = () => {
  return {
    "label": "Label",
    "name": "label",
    "widget": "string"
  }
}

export const navLogo = () => {
  return {
    "label": "Logo",
    "name": "logo",
    "type": "widget",
    "required": false
  }
}

const fields = {
  entityType,
  // identifiers
  slugId,
  customId,
  randomId,
  title,
  //
  pageType,
  pageSlug,
  // dates
  dateAuthored,
  dateStart,
  dateEnd,
  // general
  isDraft,
  body,
  description,
  // media
  mediaDescription,
  mediaScope,
  // Navigation
  navLabel,
  navLogo
}

export default fields
