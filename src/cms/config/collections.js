import * as Fields from "./fields"

export const pageLinks = () => {
  return {
    "name": "pageLinks",
    "label": "Page Links",
    "label_singular": "Page Link",
    "folder": "pages",
    "create": false,
    "delete": false,
    "hide": true,
    "nested": {
      "depth": 2
    },
    "fields": [
      Fields.pageSlug(),
      Fields.pageType(),
      Fields.title()
    ]
  };
}

export const pages = () => {
  return {
    "name": "pages",
    "label": "Pages",
    "label_singular": "Page",
    "folder": "pages",
    "create": false,
    "slug": "{{slug}}",
    "filter": {field: "pageType", value: "default"},
    "fields": [
      Fields.pageType("default"),
      Fields.pageSlug(),
      Fields.title(),
      Fields.relatedFeatureImage(),
      Fields.body()
    ]
  };
}

export const tours = () => {
  return {
    "name": "tours",
    "label": "Tours",
    "label_singular": "Tour",
    "folder": "pages",
    "create": true,
    "slug": "{{slug}}",
    "filter": {field: "pageType", value: "tour"},
    "fields": [
      Fields.pageType("tour"),
      Fields.pageSlug(),
      Fields.title(),
      Fields.relatedFeatureImage(),
      Fields.body()
    ]
  };
}

export const journals = () => {
  return {
    "name": "journals",
    "label": "Journals",
    "label_singular": "Journal",
    "folder": "pages/posts",
    "create": true,
    "slug": "{{startDate}}-{{slug}}",
    "summary": "{{startDate}}: {{title}}",
    "sortable_fields": ["startDate", "title"],
    "view_groups": [
        { "label": "Tour", "field": "collection" },
        { "label": "Drafts", "field": "draft" }
    ],
    "filter": {field: "pageType", value: "journal"},
    "fields": [
      Fields.pageType("journal"),
      Fields.pageSlug(),
      Fields.title(),
      Fields.relatedPageParentTour(),
      Fields.relatedFeatureImage(),
      Fields.dateStart(),
      Fields.dateEnd(),
      Fields.isDraft(),
      Fields.body()
    ]
  }
}

export const images = () => {
  return {
    "name": "images",
    "label": "Images",
    "label_singular": "Image",
    "folder": "media/image",
    "extension": "json",
    "create": false,
    "delete": false,
    "hide": false,
    "slug": "{{title}}",
    "summary": "{{title}} ({{scope}}) {{description}}",
    "filter": { "field": "entityType", "value": "image" },
    "sortable_fields": ["capture.localDateTime", "title"],
    "view_groups": [ { "label": "Tour", "field": "collection" }],
    "fields": [
      Fields.entityType("image"),
      Fields.title(),
      Fields.mediaDescription(),
      Fields.mediaScope(),
      Fields.relatedOwningAssetLibrary(),
      Fields.relatedTags(),
      Fields.mediaSource(),
      Fields.mediaCapture()
    ]
  }
}

export const videos = () => {
  return {
    "name": "videos",
    "label": "Videos",
    "label_singular": "Video",
    "folder": "media/video",
    "extension": "json",
    "create": false,
    "delete": false,
    "hide": false,
    "slug": "{{title}}",
    "summary": "{{title}}",
    "filter": { "field": "entityType", "value": "video" },
    "sortable_fields": ["startDate", "title"],
    "view_groups": [ { "label": "Tour", "field": "collection" }],
    "fields": [
      Fields.entityType("video"),
      Fields.title(),
      Fields.mediaDescription(),
      Fields.mediaScope(),
      Fields.relatedOwningAssetLibrary(),
      Fields.relatedTags(),
      Fields.mediaSource(),
      Fields.mediaCapture()
    ]
  }
}

export const gpsRecording = () => {
  return {
    "name": "gpsRecording",
    "label": "GPS Recordings",
    "label_singular": "GPS Recording",
    "folder": "media/gps",
    "extension": "json",
    "create": false,
    "delete": false,
    "hide": false,
    "slug": "{{title}}",
    "summary": "{{title}}",
    "filter": { "field": "entityType", "value": "gps-recording" },
    "sortable_fields": ["title"],
    "view_groups": [ { "label": "Tour", "field": "collection" }],
    "fields": [
      Fields.entityType("gps-recording"),
      Fields.title(),
      Fields.date(),
      { "label": "Sequence", "name": "sequence", "widget": "number", "min": 1, "default": 1 },
      { "label": "Start Location", "name": "begin", "widget": "string" },
      { "label": "End Location", "name": "end", "widget": "string" },
      { "label": "Distance", "name": "distance", "widget": "number", "min": 0 },
      { "label": "Transportation Mode", "name": "mode", "widget": "select", options: [ "bicycle", "train", "automobile", "boat" ] },
      { "label": "File Name", "name": "fileName", "widget": "string" },
      Fields.relatedOwningAssetLibrary()
    ]
  }
}

const tagsFile = () => {
  return {
  "name": "tags",
  "label": "Tags",
  "label_singular": "Tag",
  "file": "settings/tags.yml",
  "fields": [{
    "label": "Tags",
    "label_singular": "Tag",
    "name": "tag",
    "widget": "list",
    "fields": [
      Fields.title(),
      Fields.description()
    ]}]
  }
}

const navigationFile = () => {
  return {
  "name": "navigation",
  "label": "Navigation",
  "file": "settings/navigation.yml",
  "fields": [
      Fields.navLogo(),
      Fields.navItems(2)
    ]
  }
}

const mediaLibraryFile = () => {
  return {
    "name": "media",
    "label": "Media",
    "file": "settings/media.yml",
    "fields": [
      Fields.mediaLibraries()
    ]
  }
}

export const settings = () => {
  return {
    "name": "settings",
    "label": "Settings",
    "files": [
      navigationFile(),
      mediaLibraryFile(),
      tagsFile()
    ]
  }
}

const collections = {
  pageLinks,
  pages,
  tours,
  journals,
  images,
  videos,
  gpsRecording,
  settings
}

export default collections
