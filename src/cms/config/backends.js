export const gitlab = () => {
  return {
    "backend": {
        "name": "gitlab",
        "repo": "greg-camille-go-bike/greg-camille-go-bike-content",
        "branch": "draft",
        "auth_type": "pkce",
        "app_id": "2221aafd83a9a0e81c025bd01ec119e3593e53f1401c84110001a7e64c59fd66",
        "commit_messages": {
            "create": "Create {{collection}} “{{slug}}”",
            "update": "Update {{collection}} “{{slug}}”",
            "delete": "Delete {{collection}} “{{slug}}”",
            "uploadMedia": "[skip ci] Upload “{{path}}”",
            "deleteMedia": "[skip ci] Delete “{{path}}”"
        }
    }
  };
}

export const local = () => {
  return {
    "backend": {
        "name": "proxy",
        "proxy_url": `/api/v1`,
        "branch": "draft"
    },
    "local_backend": true
  };
}


export const auto = () => {
  return process.env.NODE_ENV === "development" ? local() : gitlab();
}

export default auto;
