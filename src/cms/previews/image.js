import * as React from 'react'

class ImagePreview extends React.Component {

  render(){
    const { entry } = this.props
    const prefix=`${process.env.GATSBY_ASSETS_PREFIX_URL||''}/source/images/`

    const description = entry.getIn(['data','description']);
    const fileName = entry.getIn(['data','source','fileName'])

    return (<figure>
              <img src={ `${prefix}/${fileName}` } alt={ description ? description : 'A picture from our tour'} style={ { 'width': '90%'} } />
             { description ? (<figcaption>{description}</figcaption>) : null }
            </figure>)
  }
}

export default ImagePreview
