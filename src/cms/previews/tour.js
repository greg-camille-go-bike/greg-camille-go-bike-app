import * as React from 'react'

import Tour from '../../templates/Tour'
import PreviewLayout from '../../components/layout/PreviewLayout'
import { getFeatureImageNode } from '../util'

class TourPreview extends React.Component {

  render() {
    const { entry, fieldsMetaData, widgetFor } = this.props;
    const title = entry.getIn(['data','title']);
    const body = widgetFor("body")

    return (
      <PreviewLayout pageTitle={ title } image={ getFeatureImageNode(entry, fieldsMetaData) }>
        <Tour body={body} />
      </PreviewLayout>
    )
  }

}

export default TourPreview
