import * as React from 'react'

import JournalTemplate from '../../templates/Journal'
import PreviewLayout from '../../components/layout/PreviewLayout'

import { getFeatureImageNode } from '../util'
import { formatDateLine } from '../../util/resolvers'

class JournalPreview extends React.Component {

  render() {
    const { entry, fieldsMetaData, widgetFor } = this.props;
    const title = entry.getIn(['data','title']);
    const startDate = entry.getIn(['data','startDate']);
    const endDate = entry.getIn(['data','endDate']);
    const body = widgetFor("body");
    const displayDateLine = formatDateLine(startDate, endDate)

    return (
      <PreviewLayout pageTitle={ title } image={ getFeatureImageNode(entry, fieldsMetaData) }>
        <JournalTemplate displayDateLine={ displayDateLine } body={body} />
      </PreviewLayout>
    )
  }

}

export default JournalPreview
