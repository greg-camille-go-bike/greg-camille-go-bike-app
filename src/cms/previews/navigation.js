import * as React from 'react'
import PreviewLayout from '../../components/layout/PreviewLayout'

class TourPreview extends React.Component {

  render() {
    const { entry } = this.props;
    const children = entry.getIn(["data", "items"]).toJS()

    return (
      <PreviewLayout pageTitle={ "Navigation Preview" } image={ null } navItems={ children } />
    )
  }

}

export default TourPreview
