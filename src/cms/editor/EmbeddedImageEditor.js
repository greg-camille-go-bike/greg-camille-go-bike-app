import * as React from 'react';
import EmbeddedImagePreview from './EmbeddedImagePreview'

export const EmbeddedImageEditor = {
    id: 'EmbeddedImage',
    label: 'Embedded Image',
    fields: [
        {
            "name": "image",
            "label": "Image",
            "widget": "relation",
            "collection": "images",
            "search_fields": [ "title" ],
            "value_field": "{{title}}",
            "display_fields": [ "title" ]
        },
        {
            "name": "caption",
            "label": "Caption",
            "widget": "string",
            "default": ""
        }
    ],
    pattern: /^<EmbeddedImage image="([^"]*)" caption="([^"]*)" \/>$/,
    fromBlock: function (match) {
        return {
            image: match[1],
            caption: match[2],
        }
    },
    toBlock: function (obj) {
        return `<EmbeddedImage image="${obj.image}" caption="${obj.caption}" />`;
    },
    toPreview: function (obj) {
        return (<EmbeddedImagePreview image={obj.image} caption={obj.caption} />);
    }
}

export default EmbeddedImageEditor;
