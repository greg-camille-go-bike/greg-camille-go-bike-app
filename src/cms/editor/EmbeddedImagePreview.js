import * as React from 'react'

export const EmbeddedImagePreview = function({image, caption}) {
    return (
      <figure>
        <div>{image}</div>
        <figcaption>{caption}</figcaption>
      </figure>
    )
}

export default EmbeddedImagePreview;
