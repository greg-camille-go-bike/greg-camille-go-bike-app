import CMS from 'netlify-cms-app'
import config from './config'

import { UuidControl, UuidPreview } from './widgets/uuid'

import ImagePreview from './previews/image'
import JournalPreview from './previews/journal'
import TourPreview from './previews/tour'
import NavigationPreview from './previews/navigation'

import EmbeddedImageEditor from './editor/EmbeddedImageEditor'

CMS.registerMediaLibrary({
  name: 'disabled',
  init: () => ({ show: () => undefined, enableStandalone: () => false }),
});

CMS.init({ config });

CMS.registerWidget("uuid", UuidControl, UuidPreview);

CMS.registerPreviewTemplate("images", ImagePreview)
CMS.registerPreviewTemplate("journals", JournalPreview)
CMS.registerPreviewTemplate("tours", TourPreview)
CMS.registerPreviewTemplate("navigation", NavigationPreview)

CMS.registerEditorComponent(EmbeddedImageEditor);
