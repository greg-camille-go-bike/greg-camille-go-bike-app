import * as React from 'react'
import { v4 } from 'uuid';

export class UuidControl extends React.Component {

  componentDidMount() {
    if(! this.props.value) {
      const id = v4();
      console.log(`Generated uuid ${id} for input ${this.props.forID}`)
      this.props.onChange(id);
    }
  }

  render() {
    const {  value, field, forID, classNameWrapper, onChange } = this.props
    return <input id={ forID } className={ classNameWrapper } value={ value } disabled />;
  }
}

export class UuidPreview extends React.Component {
  render() {
    return null;
  }
}

const exports = {
  UuidControl,
  UuidPreview
}

export default exports;
