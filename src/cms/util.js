//import { formatGatsbyImageData } from "../gatsby/schema/image"

export const getFeatureImageNode = (entry, fieldsMetaData) => {
    const featureImageId = entry.getIn(["data","featureImage"]);
    const featureImageNode = fieldsMetaData.getIn(["featureImage","images", featureImageId])?.toJS()
  // TODO gatsby's GatsbyImage component has dependencies not available in all environments causing failures
//    if(featureImageNode) {
//      featureImageNode.gatsbyImageData = formatGatsbyImageData(featureImageNode);
//    }
  return featureImageNode;
}

const util = {
  getFeatureImageNode
}
export default util
