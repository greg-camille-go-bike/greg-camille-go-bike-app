import * as React from 'react'
import PropTypes from 'prop-types'

class Modal extends React.Component {

  constructor(props) {
    super(props)
    this.handleKeyDown = this.handleKeyDown.bind(this)

    const cache = {};
    this.handleTouchStart = this.handleTouchStart.bind(this, cache);
    this.handleTouchEnd = this.handleTouchEnd.bind(this, cache)
  }

  closeModal() {
    this.props.onClose();
  }

  navigatePrevious() {
    if(typeof this.props.onPrevious === 'function'){
      this.props.onPrevious()
    }
  }

  navigateNext() {
    if(typeof this.props.onNext === 'function'){
      this.props.onNext()
    }
  }

  handleKeyDown(e) {
    if (e.keyCode === 27) {
      this.closeModal()
    }
    else if(e.keyCode === 37) {
      this.navigatePrevious()
    }
    else if(e.keyCode === 39) {
      this.navigateNext()
    }
  }

  handleTouchStart(cache, e) {
    const changed = e.changedTouches[0];
    cache[changed.identifier] = changed.pageX;
  }

  handleTouchEnd(cache, e) {
    const changed = e.changedTouches[0];
    const distance = changed.pageX - cache[changed.identifier];
    delete cache[changed.identifier]

    if(Math.abs(distance)) {
      if(distance < 0){
        this.navigateNext();
      }
      else {
        this.navigatePrevious();
      }
    }
  }

  componentDidMount() {
    document.addEventListener("keydown", this.handleKeyDown, false);
  }

  componentWillUnmount() {
    document.removeEventListener("keydown", this.handleKeyDown, false);
  }

  preventBackgroundScrolling() {
    if(this.props.children) {
      document.documentElement.classList.add("is-clipped");
    }
    else {
      document.documentElement.classList.remove("is-clipped");
    }
  }

  componentDidUpdate(prevProps){
    this.preventBackgroundScrolling()
  }

  render() {
    return (
      <div onTouchStart={ this.handleTouchStart }
           onTouchEnd={ this.handleTouchEnd }
           className={ "modal" + (this.props.isOpen ? " is-active" : "") } >
        <div className="modal-background"></div>
        <div className={this.props.contentClassName ?? 'modal-content'}>
          {this.props.children}
        </div>
        <button onClick={ this.closeModal.bind(this) } className="modal-close is-large" aria-label="close"></button>
      </div>
    )
  }
}

Modal.propTypes = {
  isOpen: PropTypes.bool.isRequired,
  onClose: PropTypes.func.isRequired,
  onNext: PropTypes.func,
  onPrevious: PropTypes.func
}

export default Modal;
