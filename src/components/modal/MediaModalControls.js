import * as React from 'react'
import PropTypes from 'prop-types'

import MediaModal from './MediaModal'

export const MediaContext = React.createContext()

export class MediaModalControls extends React.Component {

  constructor(props){
    super(props)
    this.state = {
      focusedMedia: null
    }
    this.focusMedia = this.focusMedia.bind(this)
  }

  focusMedia(media) {
    this.setState({
      focusedMedia: media
    })
  }

  render() {
    return (<MediaContext.Provider value={{ focusMedia: this.focusMedia, focusedMedia: this.state.focusedMedia }}>
       { this.props.children }
       <MediaModal
          media={ this.props.media }
          focusedMedia={ this.state.focusedMedia }
          focusMedia={ this.focusMedia } />
    </MediaContext.Provider>)
  }

}

MediaModalControls.propTypes = {
  media: PropTypes.arrayOf(PropTypes.object.isRequired).isRequired
}
