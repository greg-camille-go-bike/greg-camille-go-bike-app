import * as React from 'react'
import PropTypes from 'prop-types'

import { GatsbyImage, getImage } from 'gatsby-plugin-image'

import VideoPlayer from '../video/VideoPlayer'
import Modal from './Modal'

export default class MediaModal extends React.Component {

  constructor(props){
    super(props)

    this.onClose = this.onClose.bind(this)
    this.onNext = this.onNext.bind(this)
    this.onPrevious = this.onPrevious.bind(this)
  }

  setMedia(media) {
    if(this.props.focusMedia) {
      this.props.focusMedia(media)
    }
  }

  onClose() {
    this.setMedia(null)
  }

  findCurrentOffset(focused, media) {
    let i;
    for(i=0; i < media.length; i++) {
      const next = media[i]
      if(next.id === focused.id) {
        return i;
      }
    }
    return -1;
  }

  navigateOffset(offset) {
    const focused = this.props.focusedMedia;
    const media = this.props.media;
    if(focused && media && media.length > 0) {
      const currentIndex = this.findCurrentOffset(focused, media)
      if(currentIndex < 0) {
        this.setMedia(media[0]);
      }
      else {
        const newIndex = (media.length + currentIndex + offset) % media.length;
        this.setMedia(media[newIndex])
      }
    }
  }

  onNext() {
    this.navigateOffset(1)
  }

  onPrevious() {
    this.navigateOffset(-1)
  }

  renderContent(node) {
    if(node.gatsbyImageData) {
      const imageData = getImage(node);
      return (
        <figure>
          <GatsbyImage
                image={imageData}
                alt={node.description ? node.description : ""}
              />
          { node.description && node.description.trim().length > 0 ? (<figcaption className='has-text-light'>{node.description}</figcaption>) : null }
        </figure>)
    }
    else if(node.videoData) {
      return (
        <figure>
          <VideoPlayer sources= {node.videoData.sources} />
          { node.description && node.description.trim().length > 0 ? (<figcaption className='has-text-light'>{node.description}</figcaption>) : null }
        </figure>)
    }
    return null;
  }

  render() {
    const contentClasses = this.props.focusedMedia ? `modal-media-content media-${this.props.focusedMedia.layout.toLowerCase()}` : 'modal-content';
    return (<Modal
              isOpen={ !! (this.props.focusedMedia) }
              onClose={ this.onClose }
              onPrevious={ this.onPrevious }
              onNext={ this.onNext }
              contentClassName={ contentClasses }
            >
            { this.props.focusedMedia ? this.renderContent(this.props.focusedMedia) : null }
            </Modal>)
  }

}

MediaModal.propTypes = {
  media: PropTypes.arrayOf(PropTypes.object.isRequired).isRequired,
  focusedMedia: PropTypes.object,
  focusMedia: PropTypes.func.isRequired
}
