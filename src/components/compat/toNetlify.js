export { Link } from './Link'
import * as React from 'react'

export const Content = ({children}) => {
  return (<div className="content">{children}</div>)
}

const Components = {
  Content,
  Link
}

export default Components;
