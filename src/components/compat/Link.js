import * as React from 'react';
import PropTypes from 'prop-types';


/*
The Gatsby built-in component Link fails when used within Netlify CMS previews.
This component is intended to be API compatible so that it can be used as a stand-in for that scenario.
*/
export class Link extends React.Component {

  render() {
    const {to, children, ...rest} = this.props
    return (<a href={ to } {...rest}>{children}</a>)
  }

}

Link.propTypes = {
  to: PropTypes.string.isRequired,
  children: PropTypes.node
};

export default Link;
