export { Link } from 'gatsby';
export Content from '../markdown/Content'

const Components = {
  Content,
  Link
}

export default Components;
