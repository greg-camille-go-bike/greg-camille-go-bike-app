import * as React from 'react'
import PropTypes from 'prop-types'

// TODO pass down as prop or get from context
import Content from '../compat/Content'
import LayoutSection from '../layout/LayoutSection'

class ContentSection extends React.Component {

  render() {
    const { body, title } = this.props;
    if(body) {
      return (
        <LayoutSection id="content-section" label={title}>
          <Content content={body} />
        </LayoutSection>)
    }
    else {
      return null;
    }
  }

}

ContentSection.propTypes = {
  title: PropTypes.string,
};

export default ContentSection;
