import * as React from 'react'
import { MDXRenderer } from 'gatsby-plugin-mdx'
import { MDXProvider } from "@mdx-js/react"
import EmbeddedImage from './EmbeddedImage'

const shortcodes = {
  EmbeddedImage
}

class Content extends React.Component {

  render() {
    const { content } = this.props;
    return (<MDXProvider components={ shortcodes }>
              <div className="content">
                <MDXRenderer>{content}</MDXRenderer>
              </div>
            </MDXProvider>)
  }

}

export default Content;
