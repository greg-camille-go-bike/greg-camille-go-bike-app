import * as React from 'react'
import PropTypes from 'prop-types'
import { GatsbyImage } from 'gatsby-plugin-image'

class EmbeddedImage extends React.Component {

  render() {
    const { image, caption } = this.props;
    return (
      <figure>
        <GatsbyImage image={ image } />
        { caption ? (<figcaption>{caption}</figcaption>) : null }
      </figure>)
  }

}

EmbeddedImage.propTypes = {
  image: PropTypes.object.isRequired,
  caption: PropTypes.string
}

export default EmbeddedImage
