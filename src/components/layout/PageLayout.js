import * as React from 'react'
import PropTypes from 'prop-types';

import { useStaticQuery, graphql } from 'gatsby'
import Layout from './Layout'

const PlaceholderNavs = [
    {
        type: "group",
        id: '16e6146c-2d04-42ca-b99e-19ecb24254c0',
        label: "Tours",
        items: [
            { type: "link", id: '16e6146c-2d04-42ca-b99e-19ecb24254c1', label: "The Great Northeast Loop", url: "/tours/the-great-northeast-loop" },
            { type: "link", id: '16e6146c-2d04-42ca-b99e-19ecb24254c2', label: "Southern Spokes", url: "/tours/southern-spokes" }
        ],
    }
]

const PageLayout = (props) => {
    const data = useStaticQuery(graphql`
    query {
        site {
            siteMetadata {
                title
            }
        }
    }`)
    return (
      <Layout siteTitle={ data.site.siteMetadata.title } navItems={ PlaceholderNavs } data={data} {...props} />
    )
}

PageLayout.propTypes = {
  pageTitle: PropTypes.string.isRequired,
  image: PropTypes.object, // TODO switch object to shape
  editUrl: PropTypes.string.isRequired,
  children: PropTypes.element.isRequired
};

export default PageLayout
