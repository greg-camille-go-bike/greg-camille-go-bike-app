import * as React from 'react'
import PropTypes from 'prop-types'

export default class LayoutSection extends React.Component {

  render() {
    const { id, label, children, ...rest } = this.props;

    if(id && children) {
      return (<div id={id} className="section" { ...rest }>
                {label ? (<h3 className="title is-3">{label}</h3>) : null }
                {children}
              </div>)
    }
    return null;
  }

}

LayoutSection.propTypes = {
  id: PropTypes.string.isRequired,
  label: PropTypes.string,
  children: PropTypes.element.isRequired
}
