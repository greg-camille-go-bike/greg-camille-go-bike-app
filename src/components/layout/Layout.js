import * as React from 'react'
import PropTypes from 'prop-types';

import { GatsbyImage } from 'gatsby-plugin-image'

import "./layout.scss"

import NavBar from '../NavBar'
import EditIcon from '../admin/EditIcon'

class Layout extends React.Component {

  constructor(props) {
    super(props)
    this.scrollToStart = this.scrollToStart.bind(this);
  }

  scrollToStart() {
    window.scrollTo({top: window.innerHeight, behavior: 'smooth' })
  }

  render() {
    const { image, pageTitle, siteTitle, editUrl, navItems, children } = this.props;
    return (
      <div className="has-navbar-fixed-top">
        <title>{pageTitle} | { siteTitle }</title>
        <NavBar navItems={ navItems ? navItems : [] } />
        <main>
          <section onClick={ this.scrollToStart } className={"hero is-link is-fullheight" + (image ? " has-background":"")}>
            {
              (image ? (<GatsbyImage image={ image.gatsbyImageData } alt="" className="hero-background" />) : null)
            }
            <div className="hero-body">
              <div className="">
                <h1 className="title">{siteTitle}</h1>
                <h2 className="subtitle" >
                  {pageTitle}
                  { <EditIcon editUrl={ editUrl } /> }
                </h2>
              </div>
            </div>
          </section>
          <div className="container">
              {children}
          </div>
        </main>
      </div>
    )
  }
}

Layout.propTypes = {
  pageTitle: PropTypes.string.isRequired,
  siteTitle: PropTypes.string.isRequired,
  image: PropTypes.object, // TODO switch object to shape
  editUrl: PropTypes.string,
  navItems: PropTypes.arrayOf(PropTypes.object.isRequired).isRequired, // TODO switch object to shape
  children: PropTypes.element.isRequired
};

export default Layout
