import * as React from 'react'
import PropTypes from 'prop-types';

import Layout from './Layout'

const PreviewNavs = [
    {
      type: "group",
      id: "5613e8d9-3520-4811-b45f-7a1bfa0ba870",
      label: "Tours",
      items: [
        { type: "link", id: "5613e8d9-3520-4811-b45f-7a1bfa0ba871", label: "The Great Northeast Loop", url: "/tours/the-great-northeast-loop" },
        { type: "link", id: "5613e8d9-3520-4811-b45f-7a1bfa0ba872", label: "Southern Spokes", url: "/tours/southern-spokes" }
      ],
    },
    {
      type: "group",
      id: "5613e8d9-3520-4811-b45f-7a1bfa0ba873",
      label: "Local Rides",
      items: [
        { type: "link", id: "5613e8d9-3520-4811-b45f-7a1bfa0ba874", label: "To Bombay Hook", url: "/" },
        { type: "link", id: "5613e8d9-3520-4811-b45f-7a1bfa0ba875", label: "To Chesapeake City", url: "/" },
        { type: "link", id: "5613e8d9-3520-4811-b45f-7a1bfa0ba876", label: "To Fox Point", url: "/" },
        { type: "link", id: "5613e8d9-3520-4811-b45f-7a1bfa0ba877", label: "To Valley Forge", url: "/" }
      ],
    },
    {
      type: "link",
      id: "5613e8d9-3520-4811-b45f-7a1bfa0ba878",
      label: "About",
      about: "/url"
    }
];

class PreviewLayout extends React.Component {

  render() {
    const navs = this.props.navItems ? this.props.navItems : PreviewNavs;

    return (
      <Layout siteTitle="Greg & Camille Go Bike" navItems={ navs } {...this.props} />
    )
  }
}

PreviewLayout.propTypes = {
  pageTitle: PropTypes.string.isRequired,
  image: PropTypes.object, // TODO switch object to shape
  children: PropTypes.element.isRequired
};

export default PreviewLayout
