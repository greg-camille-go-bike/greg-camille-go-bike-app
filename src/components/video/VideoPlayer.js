import * as React from 'react'
import PropTypes from 'prop-types'
import videojs from "video.js";
import "video.js/dist/video-js.css";

const previewOptions = {
    fluid: true,
    controls: false,
    aspectRatio: "4:3",
    autoplay: "true",
    loop: true

//    controls: false,
//    autoplay: "muted",
//    loop: "true",
//    aspectRatio: "4:3"
}

const fullOptions = {
    fluid: true,
    controls: true,
    aspectRatio: "16:9"
}

export const PREVIEW_OPTIONS = {
    fluid: true,
    controls: false,
    aspectRatio: "4:3",
    autoplay: "true",
    loop: true
}

export const DEFAULT_OPTIONS = {
    fluid: true,
    controls: true,
    aspectRatio: "16:9"
}

export class VideoPlayer extends React.Component {
  componentDidMount() {
    const { sources, options=DEFAULT_OPTIONS } = this.props;

    this.player = videojs(this.videoNode, {
      sources: sources,
      ...options
    })

    /*
    const isPreview = "preview" === this.props.mode
    if("preview" === this.props.mode) {
      this.player = videojs(this.videoNode, {
        sources: this.props.sources,
        ...previewOptions
      })
    }
    else {
      this.player = videojs(this.videoNode, {
        sources: this.props.sources,
        ...fullOptions
      })
    }*/
  }

  componentWillUnmount() {
    if (this.player) {
      this.player.dispose()
    }
  }

  componentDidUpdate(prevProps) {
    // TODO maybe a more complete check although for my use case
    // any source differing indicates the full set is different
    if(this.props.sources[0].src !== prevProps.sources[0].src ) {
      this.player.src(this.props.sources);
    }
  }

  render() {
    return (
      <div className={this.props.className}>
        <div data-vjs-player>
          <video ref={ node => this.videoNode = node } className="video-js"></video>
        </div>
      </div>
    )
  }
}

VideoPlayer.propTypes = {
  sources: PropTypes.arrayOf(PropTypes.shape({
    src: PropTypes.string.isRequired,
    type: PropTypes.string.isRequired
  })).isRequired
}

export default VideoPlayer
