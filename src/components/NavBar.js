import * as React from 'react'
import { StaticImage } from "gatsby-plugin-image"

class Navbar extends React.Component {

    constructor(props) {
        super(props)
        this.state = { isActive: false }
        this.toggleActive = this.toggleActive.bind(this)
    }

    toggleActive() {
      this.setState(prev => ({
          isActive: ! prev.isActive
      }))
    }

    getActiveClass() {
        return this.state.isActive ? " is-active " : ""
    }

    renderItem(item, depth=0) {
        if(item.type === "group") {
            return (
                <div className="navbar-item has-dropdown is-hoverable" key={item.label}>
                  <a className="navbar-link">{item.label}</a>
                  <div className="navbar-dropdown">
                    { item.items.map(x => this.renderItem(x, depth+1) ) }
                  </div>
                </div>);
        } else if(item.type === "link") {
            return (<a href={item.url} className="navbar-item" key={item.label}>{item.label}</a>);
        } else if(item.type === "divider") {
            return (<hr className="navbar-divider" key={item.label}/>);
        }
        return null;
    }

    render() {
        return (
            <nav className="navbar is-fixed-top is-dark">
                <div className="navbar-brand">
                    <a href="/" className="navbar-item">
                      <StaticImage src="../images/GClogogrey.png" alt="Greg & Camille's biking blog"/>
                    </a>
                    <a role="button"
                        className={ "navbar-burger" + this.getActiveClass() }
                        aria-label="menu" aria-expanded="false"
                        onClick={this.toggleActive}>
                        <span aria-hidden="true"></span>
                        <span aria-hidden="true"></span>
                        <span aria-hidden="true"></span>
                    </a>
                </div>
                <div id="navMenu" className={ "navbar-menu" + this.getActiveClass() }>
                    <div className="navbar-start">
                      { this.props.navItems.map(x => this.renderItem(x) ) }
                    </div>
                </div>
            </nav>)
    }
}

export default Navbar
