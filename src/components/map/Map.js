import * as React from 'react'
import PropTypes from 'prop-types'

import "leaflet/dist/leaflet.css"
import "react-leaflet-markercluster/dist/styles.min.css";

import { MapContainer, GeoJSON, LayersControl, TileLayer, Marker, LayerGroup, MapConsumer } from 'react-leaflet';
import { divIcon } from "leaflet";
import MarkerClusterGroup from 'react-leaflet-markercluster';
import { MediaContext } from '../modal/MediaModalControls'

const optionsByType = {
  BICYCLE: { color: "#3388ff" },
  BOAT: { dashArray: "1 2", weight: 1, color: "grey" },
  TRAIN: { dashArray: "1 2", weight: 1, color: "brown" },
  AUTOMOBILE: { dashArray: "1 2", weight: 1, color: "green" }
}

export class Map extends React.Component {

  constructor(props){
    super(props)
    this.mapRef = React.createRef()
  }

  getRecordingsBbox(recordings) {
    return recordings.reduce((carry, next)=>{
      const nb = next.gpsData.bbox;
      if(carry) {
        carry.minX = Math.min(carry.minX, nb.minX);
        carry.minY = Math.min(carry.minY, nb.minY);
        carry.maxX = Math.max(carry.maxX, nb.maxX);
        carry.maxY = Math.max(carry.maxY, nb.maxY);
        return carry;
      }
      return nb;
    }, null)
  }

  getPhotosBbox(photos) {
    return photos
      .map(x=> x.locationData)
      .reduce((carry,next)=> {
      const ld = next;
      let bbox = {}
      if(carry) {
        bbox.minX= Math.min(ld.longitude, carry.minX);
        bbox.maxX= Math.max(ld.longitude, carry.maxX);
        bbox.minY= Math.min(ld.latitude, carry.minY);
        bbox.maxY= Math.max(ld.latitude, carry.maxY);
      }
      else {
        bbox.minX= ld.longitude;
        bbox.maxX= ld.longitude;
        bbox.minY= ld.latitude;
        bbox.maxY= ld.latitude;
      }
      return bbox;
    }, null);
  }

  getFitBounds(recordings, photos) {
    const bbox = this.isNotEmpty(recordings) ? this.getRecordingsBbox(recordings) : this.getPhotosBbox(photos);
    return [[ bbox.minY, bbox.minX ], [bbox.maxY, bbox.maxX]]
  }

  prepareRecording(recording) {
    const prepared = {
      key: recording.id,
      style: optionsByType[recording.mode],
      data: recording.gpsData.coordinates.features[0]
    }
    return prepared
  }

  getGeotaggedPhotos(){
    const media = this.props.photos ?? this.props.media ?? [];
    return media.filter(x => x.locationData && x.locationData.latitude && x.locationData.longitude )
  }

  getImageThumbnail(media) {
    const image = media.videoData ? media.videoData?.preview?.image : media;
    const src = image.gatsbyImageData?.placeholder?.fallback;
    return src ? src : image?.gatsbyImageData?.images?.sources[0]?.srcSet?.split(" ")[0];
  }

  isNotEmpty(a) {
    return a && a.length > 0
  }

  getFocusedMediaLocation() {
    const locationData = this.props?.focusedMedia?.locationData;
    if(locationData && locationData.latitude && locationData.longitude) {
      return [
        locationData.latitude,
        locationData.longitude
      ]
    }
    return null;
  }

  componentDidUpdate(prevProps, prevState) {
    if(prevProps.focusedMedia !== this.props.focusedMedia) {
      const center = this.getFocusedMediaLocation();
      if(center) {
        const currentZoom = this.state.map.getZoom();
        this.state.map.flyTo(center, Math.max(currentZoom, 11), { duration: 2 })
      }
    }
  }

  render() {
    const { recordings=[] } = this.props;
    const geotaggedPhotos = this.getGeotaggedPhotos();
    const hasData = this.isNotEmpty(recordings) || this.isNotEmpty(geotaggedPhotos)

    if(typeof window !== "undefined" && hasData) {
      const bounds = this.getFitBounds(recordings, geotaggedPhotos);
      const center=this.getFocusedMediaLocation();

      return (
        <div>
          <MapContainer center={center} bounds={ bounds } scrollWheelZoom={false}  whenCreated={map => this.setState({ map }) } >
              <LayersControl position="topright">
                <LayersControl.BaseLayer checked name="OpenStreetMap.Mapnik">
                  <TileLayer
                    attribution='&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
                    url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
                  />
                </LayersControl.BaseLayer>

              <LayersControl.BaseLayer name="CyclOSM">
                <TileLayer
                  attribution='<a href="https://github.com/cyclosm/cyclosm-cartocss-style/releases" title="CyclOSM - Open Bicycle render">CyclOSM</a> | Map data: &copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
                  url="https://{s}.tile-cyclosm.openstreetmap.fr/cyclosm/{z}/{x}/{y}.png"
                  maxZoom={20} />
              </LayersControl.BaseLayer>

              <LayersControl.BaseLayer name="Esri.NatGeoWorldMap">
                <TileLayer
                  attribution='Tiles &copy; Esri &mdash; National Geographic, Esri, DeLorme, NAVTEQ, UNEP-WCMC, USGS, NASA, ESA, METI, NRCAN, GEBCO, NOAA, iPC'
                  url="https://server.arcgisonline.com/ArcGIS/rest/services/NatGeo_World_Map/MapServer/tile/{z}/{y}/{x}"
                  maxZoom={16} />
              </LayersControl.BaseLayer>

                <LayersControl.Overlay checked name="Geotagged Photos">
                  <LayerGroup>
                    <MarkerClusterGroup maxClusterRadius={60} showCoverageOnHover={false}>
                        {
                          geotaggedPhotos
                            .map(x => {
                              return (
                                <Marker
                                  key={x.id}
                                  position={ [ x.locationData.latitude, x.locationData.longitude] }
                                  icon={ divIcon({
                                    html: `<img src="${this.getImageThumbnail(x)}" alt="${x.description}" />`,
                                    iconSize: [50,50] })}
                                  eventHandlers={{
                                    click: () => { this.props.focusMedia(x) },
                                  }}/>)
                            })
                        }
                    </MarkerClusterGroup>
                  </LayerGroup>
                </LayersControl.Overlay>
              </LayersControl>
              {
                (recordings.map(x => {
                  const prepared=this.prepareRecording(x);
                  return (<GeoJSON key={prepared.key} data={ prepared.data } style={ prepared.style } />)
                }))
              }
            </MapContainer>
          </div>
      )
    }
    else {
      return null;
    }

  }
}

Map.propTypes = {
  recordings: PropTypes.arrayOf(PropTypes.object.isRequired), // TODO narrow object
  photos: PropTypes.arrayOf(PropTypes.object.isRequired) // TODO narrow object
};

export class MediaAwareMap extends React.Component {

  render(){
    return (<MediaContext.Consumer>
        {(context) => {
          return (<Map
            {...this.props}
            focusMedia={ context.focusMedia }
                    focusedMedia={ context.focusedMedia } />)
        }
        }
      </MediaContext.Consumer>)
  }

}

export default MediaAwareMap;
