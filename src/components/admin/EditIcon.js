import * as React from 'react'
import PropTypes from 'prop-types';

import whileAdminOnly from './whileAdminOnly'
import EditLink from './EditLink'

class EditIcon extends React.Component {
  render() {
    const { className, editUrl, ...rest } = this.props
    return (<EditLink className={ className ? `icon ${className}` : "icon"} href={ editUrl }  {...rest} >
              <i className="fas fa-edit"></i>
            </EditLink>)
  }

}

EditIcon.propTypes = {
  editUrl: PropTypes.string.isRequired
};

export default whileAdminOnly(EditIcon);
