import * as React from 'react'
import PropTypes from 'prop-types';

import { formatEditLink } from '../../util/cms'
import whileAdminOnly from './whileAdminOnly'

class EditLink extends React.Component {

  render() {
    const { entityName, slug, children, href, ...props } = this.props;
    if(href) {
      return this.formatLink(href, children, props);
    }
    else if(entityName && slug) {
      const href = formatEditLink(this.props.entityName, this.props.slug)
      return this.formatLink(href, children, props);
    }
    else {
      return null;
    }
  }

  formatLink(href, children, props) {
      return  (<a href={ href } target="_blank" rel="noopener noreferrer" { ...props } > { children } </a>)
  }

}

EditLink.propTypes = {
  children: PropTypes.node.isRequired,
  href: PropTypes.string
};

export default whileAdminOnly(EditLink);
