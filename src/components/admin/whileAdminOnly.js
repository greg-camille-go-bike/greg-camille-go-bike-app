import * as React from 'react'

const isDevelop = "development" === process.env.NODE_ENV?.toLowerCase();
//const isDevelop = false;

export const whileAdminOnly = (Component) => {
  return (props={}) => {
    return isDevelop ? (<Component {...props} />) : null
  }
}

export default whileAdminOnly;
