const fs = require("fs").promises
const crypto = require('crypto');

const getFileName = (path) => {
  return path.replace(/^.*[\/\\]/, "")
}

const getFileExtension = (path) => {
  const guess = path.replace(/^.*\./, "")
  return guess.length < 6 ? `.${guess}` : "";
}

// maybe update this to not read the whole file into memory
const hashFileContents = async (path) => {
  const buffer = await fs.readFile(path)
  return crypto.createHash('sha256').update(buffer).digest('hex');
}

const getHashName = async (path) => {
    const extension = getFileExtension(path)
    const hash = await hashFileContents(path)
    return hash + extension;
}

exports.getFileName = getFileName;
exports.getFileExtension = getFileExtension;
exports.hashFileContents = hashFileContents;
exports.getHashName = getHashName;
