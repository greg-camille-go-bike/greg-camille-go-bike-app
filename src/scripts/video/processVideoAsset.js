const fs = require("fs").promises

const { extractVideoMetadata } = require('./extractVideoMetadata')
const { createVideoPreview } = require("./createVideoPreview")
const { VIDEO_SOURCE_BASEDIR, VIDEO_CONTENT_BASEDIR } = require("./constants")

async function processVideoAsset(assetPath) {
  console.log(`Processing video asset ${assetPath}`)
  const data = await fs.readFile(assetPath, 'utf8');
  const json = JSON.parse(data);
  const result = processVideoJson(json);
  return result
}

async function processVideoJson(data) {
  const hasMetadata = await withSourceMetadata(data)
  const hasPreviewVideo = await withPreviewVideo(hasMetadata)
  await updateVideoAsset(hasPreviewVideo);
}

function getSourceVideoPath(data) {
  const fileName=data?.source?.fileName
  if(fileName) {
    // TODO fetch if necessary to local path first
    const target = `${VIDEO_SOURCE_BASEDIR}/${fileName}`
//    console.log(`Using source at ${target}`)
    return target;
  }
  else {
    throw new Error("Missing required field source.fileName")
  }
}

async function withSourceMetadata(data) {
  const { source={} } = data
  if(source.fileName) {
    if(source.fileSize
       && source.height
       && source.width
       && source.duration) {
      // All metadata is already present
      return data;
    }
    else {
      const videoPath=getSourceVideoPath(data)
      const metadata = await extractVideoMetadata(videoPath);
      data.source = { ...source, ...metadata }
      return data;
    }
  }
  else {
    throw new Error("Asset metadata is missing required field source.fileName")
  }
}

async function withPreviewVideo(data) {
  const { preview={} } = data
  const { video, image } = preview

  if(video && preview) {
    return data;
  }
  else {
    const preview = await createVideoPreview(data);
    data.preview = preview;
    return data;
  }
}

async function updateVideoAsset(videoAsset) {
  const assetPath=`${VIDEO_CONTENT_BASEDIR}/${videoAsset.title}.json`;
  const data = JSON.stringify(videoAsset, null, 2);
//  console.log(`Writing updated video asset to ${assetPath}`, data);
  await fs.writeFile(assetPath, data);
}

exports.processVideoAsset = processVideoAsset;
