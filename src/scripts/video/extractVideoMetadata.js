const ffmpeg = require('fluent-ffmpeg');
const { getFileName } = require("../util/files")

async function extractVideoMetadata(videoPath) {
  const result = await callFFProbe(videoPath);
  const videoStream = result?.streams?.filter(x => x.codec_type = 'video' )[0]

  const fileName = getFileName(videoPath)

  if(videoStream) {
    return {
      fileName: fileName,
      fileSize: result.format.size,
      height: videoStream.height,
      width: videoStream.width,
      duration: result.format.duration
    }
  }
  else {
    throw new Error("Failed to find video stream");
  }
}

function callFFProbe(videoPath) {
  return new Promise((resolve, reject) => {
    ffmpeg.ffprobe(videoPath, (err, metadata) => {
      if(err) {
        reject(err)
      }
      else {
        resolve(metadata)
      }
    })
  })
}

exports.extractVideoMetadata = extractVideoMetadata;
