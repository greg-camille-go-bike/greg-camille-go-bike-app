const ffmpeg = require('fluent-ffmpeg');
const fs = require("fs").promises

const { extractVideoMetadata } = require('./extractVideoMetadata')
const { hashFileContents, getHashName } = require('../util/files')
const { importImageAsset } = require("../image/importImageAsset")

const { VIDEO_SOURCE_BASEDIR, VIDEO_STAGE_BASEDIR, VIDEO_TEMP_BASEDIR } = require("./constants")
const { IMAGE_TEMP_BASEDIR } = require("../image/constants")
const { v4 } = require("uuid")

async function createVideoPreview(assetData) {
  const videoPath = `${VIDEO_SOURCE_BASEDIR}/${assetData.source.fileName}`

  const result = await createVideoPreviewInternal(videoPath, assetData.source)

  const previewTitle = assetData.title.replace(/\.[^.]*$/, "") + "_preview.png"

  const previewImageAssetData = await importImageAsset(result.imagePath, {
    title: previewTitle,
    library: assetData.library,
    scope: "hidden",
    capture: assetData.capture
  })

  const stagedVideos = await Promise.all(result.videos.map( x=> stageVideo(x)))

  const previews = {
    image: previewImageAssetData.title,
    videos: stagedVideos
  }

  return previews;
}

async function createVideoPreviewInternal(videoPath, sourceMetadata) {
  const metadata = sourceMetadata ? sourceMetadata : (await extractVideoMetadata(videoPath))

  const crop = metadata.width > metadata.height ? 'ih/3*4:ih' : 'iw:iw/4*3';
  const trim = getTrim(metadata)

  const title = metadata.fileName.replace(/\.[^.]*$/,'')

  const outputMp4Path = `${VIDEO_TEMP_BASEDIR}/${v4()}.mp4`;
  const outputWebmPath = `${VIDEO_TEMP_BASEDIR}/${v4()}.webm`;
  const outputImagePath = `${IMAGE_TEMP_BASEDIR}/${v4()}.png`;

  const command = ffmpeg(videoPath)
    .complexFilter(
      [
        `trim=${trim},setpts=PTS-STARTPTS,crop=${crop},split=2[v1][v2]`,
        '[v1]scale=w=320:h=240,split=2[pv1][pv2]',
        '[v2]trim=end_frame=1[pimage]'
      ]
    )
    .output(outputMp4Path)
      .outputOptions([
        '-map [pv1]',
        '-an',
        '-c:v libx264',
        '-profile:v baseline',
        '-level:v 1.3',
        '-y'
      ])
    .output(outputWebmPath)
      .outputOptions([
        '-map [pv2]',
        '-an',
//        '-c:a opus',
        '-c:v libvpx-vp9',
        '-y'
      ])
    .output(outputImagePath)
      .outputOptions([
        '-map [pimage]',
        '-y'
      ]);
  const result = await runFfmpeg(command);

  // TODO if extractVideoMetadata is updated to reliably identify codecs, then lift this out of the function and just return image/video paths
  // Maybe use mux to get the video codec for mp4 https://github.com/videojs/mux.js?
  const mp4Metadata = await extractVideoMetadata(outputMp4Path);
  const webmMetadata = await extractVideoMetadata(outputWebmPath);

  return {
    imagePath: outputImagePath,
    videos: [
      {
        path: outputMp4Path,
        ...mp4Metadata,
        mediaType: "video/mp4",
        codecs: "avc1.42000d",
        fileName: await getHashName(outputMp4Path)
      },
      {
        path: outputWebmPath,
        ...webmMetadata,
        mediaType: "video/webm",
        codecs: "vp9",
        fileName: await getHashName(outputWebmPath)
      }
    ]
  }

}

function getTrim(metadata, targetDuration=4) {
  const maxDuration = metadata.duration;

  if(maxDuration < targetDuration) {
    return `start=0:duration=${maxDuration}`;
  }
  else {
    const start = (maxDuration/2) - (targetDuration/2)
    return `start=${start}:duration=${targetDuration}`
  }
}

async function stageVideo({ path, fileName, ...rest }) {
  const target = `${VIDEO_STAGE_BASEDIR}/${fileName}`
  const moved = await fs.rename(path, target);

  return {
    fileName,
    ...rest
  }
}

async function runFfmpeg(command) {
  return new Promise((resolve, reject) => {
    return command
    // TODO conditional logging
//      .on('start', (commandLine) => { console.log(`Executing ffmpeg with: ${commandLine}`) })
      .on('error', reject)
      .on('end', resolve)
      .run();
  })
}

exports.createVideoPreview = createVideoPreview
