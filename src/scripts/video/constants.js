const { ASSETS_BASEDIR, ASSETS_SOURCE_BASEDIR, ASSETS_STAGE_BASEDIR, ASSETS_STAGE_SOURCE_BASEDIR, ASSETS_TEMP_BASEDIR, ASSET_CONTENT_BASEDIR } = require("../util/constants")
const VIDEO_DIRECTORY_NAME = "video"

exports.VIDEO_DIRECTORY_NAME = VIDEO_DIRECTORY_NAME;
exports.VIDEO_BASEDIR = `${ASSETS_BASEDIR}/${VIDEO_DIRECTORY_NAME}`;
exports.VIDEO_SOURCE_BASEDIR = `${ASSETS_SOURCE_BASEDIR}/${VIDEO_DIRECTORY_NAME}`;
exports.VIDEO_STAGE_BASEDIR = `${ASSETS_STAGE_BASEDIR}/${VIDEO_DIRECTORY_NAME}`;
exports.VIDEO_STAGE_SOURCE_BASEDIR = `${ASSETS_STAGE_SOURCE_BASEDIR}/${VIDEO_DIRECTORY_NAME}`;
exports.VIDEO_TEMP_BASEDIR = `${ASSETS_TEMP_BASEDIR}/${VIDEO_DIRECTORY_NAME}`
exports.VIDEO_CONTENT_BASEDIR = `${ASSET_CONTENT_BASEDIR}/${VIDEO_DIRECTORY_NAME}`
