const findLocationDataForTimestamps = require('./searchLocationData').findLocationDataForTimestamps

const fs = require("fs");

const gpx_basedir=null;
const media_asset_dir=null;
// Originally intended as a throw away script, but it might be worth cleaning up for future use
throw new Error("Make sure to set gpx_basedir and media_asset_dir")

const gpxFiles = fs.readdirSync(gpx_basedir)
                   .map(x => gpx_basedir + x);

Promise.all(
  fs.readdirSync(media_asset_dir)
  .map(filename => {
    return new Promise((resolve, reject) => {
        fs.readFile(`${media_asset_dir}/${filename}`, 'utf8', (err, data) => {
            if(err) {
                reject(err)
            }
            else {
              const capture = JSON.parse(data).capture
              resolve({filename: filename, location: capture.location, dateTime: capture.dateTime})
            }
        })
    })
  })
).then(all => {
  const needles = all
     .filter(x => { return ! x.location })
     .reduce((carry, next) => {
       carry[next.filename] = next.dateTime;
       return carry;
     }, {})

  const matches = findLocationDataForTimestamps(needles, gpxFiles)

  return Object.keys(matches)
        .filter(x => !! matches[x])
        .reduce((carry, next) => {
          const match = matches[next].match
          let location = `${match.lon},${match.lat}`
          if(match.ele) {
            location = `${location},${match.ele}`
          }
          else {
            console.warn("Elevation data not found for ${next}")
          }
          const geojson = `{\"type\":\"Point\",\"coordinates\":[${location}]}`
          console.log(`match ${next} / distance ${matches[next].distance / 60000}`)
          carry[next] = geojson;
          return carry;
        }, {})
}).then(updates => {
    Object.keys(updates)
        .map(filename=> {
        const path = `${media_asset_dir}/${filename}`
        const data = fs.readFileSync(path, 'utf8')
        const json = JSON.parse(data);
        json.capture.location = updates[filename];
        const toWrite=JSON.stringify(json, null, 2);
        fs.writeFileSync(path, toWrite)
    });
})
