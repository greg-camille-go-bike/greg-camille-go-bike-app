const fs = require("fs");
const xmldom = require("xmldom")

function getChildren(node) {
  const children = node.childNodes;
  return Array.from(Array(children.length).keys()).map(x => children[x]);
}

function getChildrenType(node, nodeName) {
  return getChildren(node)
  .filter(x => x.nodeName === nodeName)
}

function findChildValue(node, tagName) {
  const maybeNode = getChildrenType(node, tagName)[0]
  return null != maybeNode ? maybeNode.firstChild.nodeValue : null;
}

function formatTrackPoint(node) {
  if( ! node.getAttribute ) {
    return null;
  }

  const time = findChildValue(node, 'time')
  return {
    lat: node.getAttribute('lat'),
    lon: node.getAttribute('lon'),
    ele: findChildValue(node, 'ele'),
    time: time,
    epoch: Date.parse(time)
  }
}

function getLocationDataForGpx(gpxPath) {
  let gpxContents = fs.readFileSync(gpxPath, 'utf8');
  let dom = new xmldom.DOMParser().parseFromString(gpxContents);
  return getLocationDataForDom(dom);
}

function getLocationDataForDom(dom) {
  return getChildrenType(dom, 'gpx')
  .flatMap(x => getChildrenType(x, 'trk') )
  .flatMap(x => getChildrenType(x, 'trkseg') )
  .flatMap(x => getChildren(x, 'trkpt') )
  .map(formatTrackPoint)
  .filter(x=> !!x && !!x.time)
}

function createLookup(gpxPaths) {
  return gpxPaths
           .flatMap(x => getLocationDataForGpx(x))
           .sort((a,b) => a.epoch > b.epoch )
}

/**
 * Binary search, but as a failed match doesn't mean that the currently tested value
 * isn't actually the closest, it is consider inclusive of the next range to be checked.
 * Once we've narrowed the range down to just a pair of values, we find which is actually closest.
 * */
function searchClosest(needle, lookup) {
  let min=0
  let max=lookup.length
  let i=0

  while(Math.abs(max-min) > 1 && i < 100000) {
    i++;
    const mid=Math.round((max-min)/2) + min
    const check=lookup[mid]

    if(needle === check.epoch) {
      return check;
    }
    else if(needle > check.epoch) {
      min=mid
    }
    else {
      max=mid
    }
  }
  if(i >= 100000) {
    console.error(`min: ${min} max: ${max}`)
    throw new Error("Exhausted search attempts")
  }

  const minDiff = Math.abs(needle - lookup[min].epoch)
  const maxDiff = Math.abs(lookup[max].epoch - needle)
  const match = maxDiff < minDiff ? lookup[max] : lookup[min]

  return match;
}

function findLocationDataForTimestamps(searchDatesByIdentifier, searchFiles, tolerance=60000) {
  const lookup = createLookup(searchFiles)

  return Object.keys(searchDatesByIdentifier)
        .reduce((carry, next) => {
          const needle = Date.parse(searchDatesByIdentifier[next]);
          const match = searchClosest(needle, lookup);

          const distance = Math.abs(needle - match.epoch)

          if(distance < tolerance) {
            carry[next] = { search: searchDatesByIdentifier[next], distance: distance, match: match }
          }
          else {
            carry[next] = null
          }

          return carry;
        }, {})
}

exports.findLocationDataForTimestamps = findLocationDataForTimestamps;
