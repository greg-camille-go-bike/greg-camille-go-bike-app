const fs = require("fs").promises
const sharp = require("sharp");
const crypto = require('crypto');
const { v4 } = require('uuid');

const {
  DEFAULT_IMAGE_SCALES,
  IMAGE_CONTENT_BASEDIR,
  IMAGE_SOURCE_BASEDIR,
  IMAGE_STAGE_BASEDIR,
  IMAGE_STAGE_SOURCE_BASEDIR,
  IMAGE_TEMP_BASEDIR,
  TYPES,
  WEBP_MAX_SIZE } = require("./constants")

async function processImageAssetFile(imageAssetPath) {
  const data = await fs.readFile(imageAssetPath, 'utf8');
  const imageAsset = JSON.parse(data);
  return await processImageAsset(imageAsset);
}

async function processImageAsset(imageAsset) {
  const hasPreviews = await withPreviews(imageAsset);
  const hasTransforms = await withTransforms(hasPreviews);
  return hasTransforms;
}

async function withPreviews(imageAsset) {
  return createPreview(imageAsset);
}

async function createPreview(imageAsset) {
  const sourceImagePath = await getSourcePath(imageAsset);
  const transform = {
    width: 25,
    type: TYPES.JPEG,
    blur: true
  }
  const results = await createDerivativeImages(sourceImagePath, [ transform ]);

  const generatedMetadata = await Promise.all(results.map( stageResultTransform ))
  addTransforms(imageAsset, "blur", generatedMetadata);
  return imageAsset;
}

async function withTransforms(imageAsset) {
  const generatedImages = await createResponsiveImageSet(imageAsset);
  const generatedMetadata = await Promise.all(generatedImages.map(stageResultTransform))
  addTransforms(imageAsset, "noop", generatedMetadata);
  return imageAsset;
}

function addTransforms(imageAsset, config, toAdd) {
  imageAsset.transforms = imageAsset.transforms ?? {}
  const forConfig = imageAsset.transforms[config] ?? {};
  imageAsset.transforms[config] = forConfig;

  for(let i = 0; i < toAdd.length; i++) {
    const next = toAdd[i];
    const { fileName, type, ...rest } = next

    const versions = forConfig[type.mediaType] ?? [];
    forConfig[type.mediaType] = versions;

    versions.push({
      fileName,
      mediaType: type.mediaType,
      ...rest
    });
  }
}

// TODO update to accomodate a configurable image set request (cropping, blur, scales, types etc)
async function createResponsiveImageSet(imageAsset) {
  const transforms = imageAsset?.transforms ?? {};
  const config = "noop";

  const required = buildTransforms(imageAsset.source, DEFAULT_IMAGE_SCALES, [ TYPES.JPEG, TYPES.WEBP, TYPES.AVIF ], config );
  const missing = required.filter(x => ! findMatchingTransform(transforms, x))

  const sourceImagePath = await getSourcePath(imageAsset);
  const resultTransforms = await createDerivativeImages(sourceImagePath, missing);

  return resultTransforms;
}

function findMatchingTransform(transforms, request) {
  const {config, type, width} = request
  const TOLERANCE = 0.1;
  const forType = transforms?.[config]?.[mediaType] ?? [];

  const matches = (forType).filter(t => Math.abs(t.width - width) < (width * TOLERANCE) );
  return matches && matches.length > 0 ? matches[0] : null;
}

function buildTransforms(source, scales, types, config) {
  // TODO check max width and estimated height for all formats
  return types
        .flatMap(x=> {
          const sizes = scales
            .map(s => Math.round(source.width * s) )
            .map(w => adjustFormatSize(x, w))
          return Array.from(new Set(sizes))
                      .map(w => { return {
                        width: w,
                        type: x,
                        config: config
                      } ;});
        })
}

async function stageResultTransform({path, type, hash, ...rest}) {
  const fileName = `${hash}.${type.extension}`
  await fs.rename(path, `${IMAGE_STAGE_BASEDIR}/${fileName}`)

  return {
    fileName: fileName,
    type,
    ...rest
  }
}

function adjustFormatSize(type, width) {
  if(type.maxSize) {
    return Math.min(type.maxSize, width);
  }
  else {
    return width;
  }
}

async function getSourcePath(imageAsset) {
  const primary = `${IMAGE_SOURCE_BASEDIR}/${imageAsset.source.fileName}`;
  const alternate = `${IMAGE_STAGE_SOURCE_BASEDIR}/${imageAsset.source.fileName}`;

  // TODO fall back to fetching from s3 if both fail
  return validateFileAtPath(primary).catch(e => validateFileAtPath(alternate))
}

async function validateFileAtPath(path) {
  return fs.stat(path).then(s => path)
}

async function createDerivativeImages(path, transforms) {
  try {

    const inputBuffer = await fs.readFile(path)
    const basePipeline = sharp(inputBuffer, { failOnError: false })

    const outputs = await Promise.all(transforms.map(async x => {

      let pipeline = basePipeline.clone()

      pipeline = pipeline.resize(x.width, x.height, {
        position: 17,
        fit: 'cover',
        background: null
      })
      .jpeg({
        quality: null,
        progressive: true,
        mozjpeg: true,
        force: x.type === TYPES.JPEG
      })
      .webp({
        quality: null,
        force: x.type === TYPES.WEBP
      })
      .avif({
        quality: null,
        force: x.type === TYPES.AVIF
      })

      if(x.blur){
        pipeline = pipeline.blur();
      }

      const {data, info} = await pipeline.toBuffer({ resolveWithObject: true });

      const hash = crypto.createHash('sha256').update(data).digest('hex');

      const path = `${IMAGE_TEMP_BASEDIR}/${v4()}.${x.type.extension}`
      const details = {
        path: path,
        width: info.width,
        height: info.height,
        size: info.size,
        hash: hash,
        type: x.type
      }

      const result = await fs.writeFile(path, data);

      return details;
    }))

    return outputs;
  } catch(e) {
    console.log("Failed with error", e)
    return [];
  }
}

exports.processImageAsset = processImageAsset;
exports.processImageAssetFile = processImageAssetFile
