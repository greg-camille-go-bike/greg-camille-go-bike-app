const { ASSETS_BASEDIR,
        ASSETS_SOURCE_BASEDIR,
        ASSETS_STAGE_BASEDIR,
        ASSETS_STAGE_SOURCE_BASEDIR,
        ASSETS_TEMP_BASEDIR,
        ASSET_CONTENT_BASEDIR } = require("../util/constants")

const DEFAULT_IMAGE_SCALES = [0.125,0.25,0.5,1]

const IMAGE_DIRECTORY_NAME = "images"

const IMAGE_TYPE_PNG = {
  mediaType: "image/png",
  extension: "png"
}

const IMAGE_TYPE_AVIF = {
  mediaType: "image/avif",
  extension: "avif"
}

const IMAGE_TYPE_WEBP = {
  mediaType: "image/webp",
  extension: "webp",
  maxSize: 16383
}

const IMAGE_TYPE_JPEG = {
  mediaType: "image/jpeg",
  extension: "jpg"
}

const IMAGE_TYPE_GIF = {
  mediaType: "image/gif",
  extension: "gif"
}

const WEBP_MAX_SIZE = 16383

exports.DEFAULT_IMAGE_SCALES = DEFAULT_IMAGE_SCALES;

exports.IMAGE_BASEDIR = `${ASSETS_BASEDIR}/${IMAGE_DIRECTORY_NAME}`;
exports.IMAGE_CONTENT_BASEDIR = `${ASSET_CONTENT_BASEDIR}/image`;
exports.IMAGE_DIRECTORY_NAME = IMAGE_DIRECTORY_NAME;
exports.IMAGE_SOURCE_BASEDIR = `${ASSETS_SOURCE_BASEDIR}/${IMAGE_DIRECTORY_NAME}`;
exports.IMAGE_STAGE_BASEDIR = `${ASSETS_STAGE_BASEDIR}/${IMAGE_DIRECTORY_NAME}`;
exports.IMAGE_STAGE_SOURCE_BASEDIR = `${ASSETS_STAGE_SOURCE_BASEDIR}/${IMAGE_DIRECTORY_NAME}`;
exports.IMAGE_TEMP_BASEDIR = `${ASSETS_TEMP_BASEDIR}/${IMAGE_DIRECTORY_NAME}`

exports.TYPES = {
  AVIF: IMAGE_TYPE_AVIF,
  GIF: IMAGE_TYPE_GIF,
  JPEG: IMAGE_TYPE_JPEG,
  PNG: IMAGE_TYPE_PNG,
  WEBP: IMAGE_TYPE_WEBP
};
