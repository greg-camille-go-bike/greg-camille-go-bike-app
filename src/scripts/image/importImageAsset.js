const { extractImageMetadata, extractCaptureMetadata } = require("./extractImageMetadata")
const { getFileName, getHashName } = require("../util/files")
const { IMAGE_STAGE_SOURCE_BASEDIR, IMAGE_CONTENT_BASEDIR } = require("./constants")

const { processImageAsset } = require("./processImageAsset")

const fs = require("fs").promises

async function importImageAsset(imagePath, options={}) {
  let imageAsset = await buildImageAsset(imagePath, options);
  await stageSourceImage(imagePath, imageAsset.source.fileName);
  // TODO maybe do this after writing the source asset?
  imageAsset = await processImageAsset(imageAsset);
  await writeImageAsset(imageAsset);

  return imageAsset;
}

async function buildImageAsset(imagePath, options) {

  const title = options.title ? options.title : getFileName(imagePath)
  const library = options.library ?? "default";
  const scope = options.scope ?? "default"

  const stats = await extractImageMetadata(imagePath);
  const capture = options.capture ?? (await extractCaptureMetadata(imagePath))

  const targetFilename = await getHashName(imagePath)

  return {
    title,
    entityType: "image",
    library,
    scope,
    source: {
      fileName: targetFilename,
      // TODO update me
      mediaType: "image/png",
      fileSize: stats.fileSize,
      height: stats.height,
      width: stats.width,
    },
    capture
  }
}

async function stageSourceImage(sourcePath, targetName) {
  const destinationPath = `${IMAGE_STAGE_SOURCE_BASEDIR}/${targetName}`;
//  console.log(`Staging ${sourcePath} to ${destinationPath}`);
  await fs.rename(sourcePath, destinationPath);
}

async function writeImageAsset(imageAsset) {
  const assetPath=`${IMAGE_CONTENT_BASEDIR}/${imageAsset.title}.json`;
  const data = JSON.stringify(imageAsset, null, 2);
//  console.log(`Writing image asset to ${assetPath}`, data);
  await fs.writeFile(assetPath, data);
}

exports.importImageAsset = importImageAsset;
