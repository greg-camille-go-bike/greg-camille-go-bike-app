const fs = require("fs").promises
const sharp = require("sharp")

async function extractImageMetadata(imagePath) {
  const stats = await sharp(imagePath).metadata()
  const fileSize = (await fs.stat(imagePath)).size

  return {
    fileSize: fileSize,
    height: stats.height,
    width: stats.width
  };
}

async function extractCaptureMetadata(imagePath) {
  // TODO implement me
  return {

  };
}

exports.extractImageMetadata = extractImageMetadata;
exports.extractCaptureMetadata = extractCaptureMetadata;
