const sharp = require("sharp");
const fs = require("fs").promises
const crypto = require('crypto');

const IMAGE_ASSETS="./src/content/media/"
const ASSET_DIR="../assets/source/"
const OUTPUT_DIR="./temp/"

async function processImages(){
    //const imageNames = await fs.readdir(IMAGE_ASSETS)

    const imageNames = [
      "IMG_20211015_140830053.jpg.json", //(@21824)
      "IMG_20211024_174815648.jpg.json", //(@22644)
      "IMG_20211116_160932967.jpg.json", //(@22376)
      "IMG_20211204_130510871.jpg.json", //(@20136)
      "IMG_20211206_141445166.jpg.json" //(@22472)
    ]

    const imagePaths = imageNames.map(x =>   IMAGE_ASSETS + x)

    let i = 0;
    while(i < imagePaths.length) {
      const next = imagePaths[i];
      console.log(`Processing ${next}`)
      const result = await processImage(next)
      console.log(`Processing was ${result}`)
      i++;
    }
}



const sizes = [0.125,0.25,0.5,1]
const WEBP_MAX_SIZE = 16383
async function processImage(imagePath) {
  const data = await fs.readFile(imagePath, 'utf8')
  const json = JSON.parse(data);
  if(json.entityType === 'image') {
    const transforms = sizes
      .map(x => Math.round(json.source.width * x))
      .flatMap(x => {
        return [{
          width: x,
          format: 'jpg'
        },
        {
          width: x,
          format: 'webp'
        },
        {
          width: x,
          format: 'avif'
        }]
      })
      .map(x=> {
        if(x.format === 'webp' && x.width > WEBP_MAX_SIZE) {
          if(WEBP_MAX_SIZE > Math.round(x.width * 0.5) ) {
            return {
              width: WEBP_MAX_SIZE,
              format: 'webp'
            }
          }
          else {
            return null;
          }
        }
        else {
          return x;
        }
      })
      .filter(x => !!x)
    transforms.push({
      width: 25,
      format: 'jpg',
      base64: true
    })

    const sourceImagePath = `${ASSET_DIR}/${json.source.fileName}`
    const resultTransforms = await processTransforms(sourceImagePath, transforms)

    json.transforms = json.transforms ? json.transforms.concat(resultTransforms) : resultTransforms;
    const result = await fs.writeFile(`${OUTPUT_DIR}/json/${json.title}.json`, JSON.stringify(json, null, 2));
    return "successful"
  }
  return "skipped"
}

async function processTransforms(path, transforms) {
  try {
    
    const inputBuffer = await fs.readFile(path)
    const basePipeline = sharp(inputBuffer, { failOnError: false })

    const outputs = await Promise.all(transforms.map(async x => {

      let pipeline = basePipeline.clone()

      pipeline = pipeline.resize(x.width, x.height, {
        position: 17,
        fit: 'cover',
        background: null
      })
      .jpeg({
        quality: null,
        progressive: true,
        mozjpeg: true,
        force: x.format === 'jpg'
      })
      .webp({
        quality: null,
        force: x.format === 'webp'
      })
      .avif({
        quality: null,
        force: x.format === 'avif'
      })

      if(x.blur){
        pipeline = pipeline.blur();
      }

      const {data, info} = await pipeline.toBuffer({ resolveWithObject: true });

      const hash = crypto.createHash('sha256').update(data).digest('hex');

      const details = {
        width: info.width,
        height: info.height,
        size: info.size,
        format: x.format
      }

      if(! x.base64) {
        details.fileName = `${hash}.${x.format}`
        const result = await fs.writeFile(`${OUTPUT_DIR}/assets/${details.fileName}`, data);
      }
      else {
        // TODO update mime type based on format
        details.content = 'data:image/jpeg;base64,' + data.toString('base64')
      }

      return details;
    }))

    return outputs;
  } catch(e) {
    console.log("Failed with error", e)
    return [];
  }
}

processImages()
