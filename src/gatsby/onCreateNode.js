
const EDIT_ALLOWED = true

function formatTourNode({ node, actions, createNodeId, createContentDigest }) {

  const content = {
    pageType: node.frontmatter.pageType,
    pageSlug: node.frontmatter.pageSlug,
    pageParent: node.frontmatter.pageParent,
    library: node.frontmatter.library,
    title: node.frontmatter.title,
    status: node.frontmatter.status.toUpperCase(),
    featureImage: node.frontmatter.featureImage
  }

  const tourNode = {
    ...content,
    id: createNodeId(`${node.id} >>> Tour`),
//    id: node.frontmatter.id,
    children: [],
    parent: node.id,
    internal: {
      type: "Tour",
      contentDigest: createContentDigest(content)
    }
  }

  return tourNode
}

function formatJournalNode({ node, actions, createNodeId, createContentDigest }) {
  const content = {
    pageType: node.frontmatter.pageType,
    pageSlug: node.frontmatter.pageSlug,
    pageParentId: node.frontmatter.pageParent,
    pageParent: node.frontmatter.pageParent,
    library: node.frontmatter.library,
    title: node.frontmatter.title,
    draft: node.frontmatter.draft,
//    collection: node.frontmatter.pageParent,
    featureImage: node.frontmatter.featureImage,
    startDate: node.frontmatter.startDate,
    endDate: !!node.frontmatter.endDate ? node.frontmatter.endDate : null
  }

  const journalNode = {
    ...content,
    id: createNodeId(`${node.id} >>> Journal`),
    children: [],
    parent: node.id,
    internal: {
      type: "TourJournal",
      contentDigest: createContentDigest(content)
    }
  }

  return journalNode
}

function formatPageNode({ node, actions, createNodeId, createContentDigest }) {
  const content = {
    pageType: node.frontmatter.pageType,
    pageSlug: node.frontmatter.pageSlug,
    pageParent: node.frontmatter.pageParent,
    library: node.frontmatter.library,
    title: node.frontmatter.title,
    featureImage: node.frontmatter.featureImage
  }

  const pageNode = {
    ...content,
    id: createNodeId(`${node.id} >>> Page`),
    children: [],
    parent: node.id,
    internal: {
      type: "StaticPage",
      contentDigest: createContentDigest(content)
    }
  }

  return pageNode
}

function formatMediaItemNode( { node, actions, createNodeId, createContentDigest }) {
  const location = node.capture.location ? JSON.parse(node.capture.location).coordinates : []
  const type = node.entityType === "video" ? "VideoAsset" : "PhotoAsset"

  /*
  const transforms = (node.transforms) ? node.transforms
        .map(x => {
          return {
              fileName: x.fileName,
              fileSize: x.size,
              width: x.width,
              height: x.height,
              format: x.fileName? x.fileName.split(".").slice(-1)[0] : null,
              content: x.content
            }
        }) : []
  */

  const content = {
    collection: node.library, // TODO delete after switch to library
    library: node.library,
    mediaType: node.entityType.toUpperCase(),
    title: node.title,
    source: {
      fileName: node.source.fileName,
      fileSize: node.source.fileSize,
      width: node.source.width,
      height: node.source.height
    },
    locationData: {
      date: node.capture.dateTime,
      localDate: node.capture.localDateTime,
      rawLocation: node.capture.location,
      latitude: location[1],
      longitude: location[0],
      altitude: location[2]
    },
    description: node.description ? node.description : "",
    scope: (node.scope ? node.scope : "unknown").toUpperCase(),
    createDateTime: node.capture.localDateTime,
    createLocalDate: node.capture.localDateTime,
    transforms: node.transforms ?? {},
    preview: node.preview ?? {}
  }

  const childNode = {
    ...content,
    id: createNodeId(`${node.id} >>> ${type}`),
    children: [],
    parent: node.id,
    internal: {
      type: type,
      contentDigest: createContentDigest(content)
    }
  }

  return childNode
}


function formatGpsNode( { node, actions, createNodeId, createContentDigest }) {
  const content = {
    collection: node.library, // TODO delete after switch to library
    library: node.library,
    mode: node.mode.toUpperCase(),
    title: node.title,
    begin: node.begin,
    end: node.end,
    date: node.date,
    sequence: node.sequence,
    distance: node.distance,
    fileName: node.fileName,
    createDateTime: node.date,
    createLocalDate: node.date
  }

  const childNode = {
    ...content,
    id: createNodeId(`${node.id} >>> LocationRecording`),
    children: [],
    parent: node.id,
    internal: {
      type: "LocationRecording",
      contentDigest: createContentDigest(content)
    }
  }

  return childNode
}

function formatLibraryNode({ library, node, actions, createNodeId, createContentDigest  }) {

  const content = {
    libraryId: library.id,
    title: library.title
  }

  const childNode = {
    ...content,
    id: createNodeId(`${node.id} ${library.id} >>> MediaLibrary`),
    children: [],
    parent: node.id,
    internal: {
      type: "MediaLibrary",
      contentDigest: createContentDigest(content)
    }
  }

  return childNode
}

exports.onCreateNode = (api ) => {
  const { node, actions, createNodeId, createContentDigest } = api;
  const { createNode, createNodeField, createParentChildLink } = actions
  if(node.internal.type === "Mdx") {
    if(node.frontmatter.pageType === "tour") {
        const tour = formatTourNode({node, actions, createNodeId, createContentDigest})
        createNode(tour)
        createParentChildLink({parent: node, child: tour})
    }
    else if(node.frontmatter.pageType === "journal") {
        const journal = formatJournalNode({node, actions, createNodeId, createContentDigest})
        createNode(journal)
        createParentChildLink({parent: node, child: journal})
    }
    else if(node.frontmatter.pageType === "default") {
        const page = formatPageNode({node, actions, createNodeId, createContentDigest})
        createNode(page)
        createParentChildLink({parent: node, child: page})
    }
  }
  else if(node.internal.type === "ImageJson") {
    const mediaItem = formatMediaItemNode({node, actions, createNodeId, createContentDigest})
    createNode(mediaItem)
    createParentChildLink({parent: node, child: mediaItem})
  }
  else if(node.internal.type === "VideoJson") {
    const mediaItem = formatMediaItemNode({node, actions, createNodeId, createContentDigest})
    createNode(mediaItem)
    createParentChildLink({parent: node, child: mediaItem})
  }
  else if(node.internal.type === "GpsJson") {
    const gps = formatGpsNode( {node, actions, createNodeId, createContentDigest} )
    createNode(gps)
    createParentChildLink({parent: node, child: gps})
  }
  else if(node.internal.type === "SettingsYaml") {
    if(node.settingsType === "media") {
      for (let i in node.libraries) {
        const library = node.libraries[i]
        const child = formatLibraryNode( {library, node, actions, createNodeId, createContentDigest} )
        createNode(child)
        createParentChildLink({parent: node, child: child})
      }
    }
  }
}
