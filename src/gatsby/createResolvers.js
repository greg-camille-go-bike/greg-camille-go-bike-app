// TODO delete, but keeping around temporarily for reference
const createCollectionResolvers = (api) => {
  const { createResolvers, intermediateSchema } = api
  const collectionTypes = intermediateSchema.getImplementations({"name": "Collection"}).objects;
  const collectionAssetTypes = intermediateSchema.getImplementations({"name": "CollectionAsset"}).objects;
  const resolvers = collectionTypes.reduce((carry, next)=> {
    carry[next] = {
      assets: {
        type: '[CollectionAsset!]',
        args: {
          "type": "String"
        },
        resolve: async (source, args, context, info) => {
          const type = args.type ? args.type : "CollectionAsset";
          const { entries } = await context.nodeModel.findAll({
            type: type,
          })
          return entries.filter(asset => {
              return asset.library === source.id;
          } )
        }
      }
    };

    return carry;
  }, {});

  createResolvers(resolvers)
}

exports.createResolvers = (api) => {
  const { createResolvers } = api

  const resolvers = {
    Mdx: {
      relatedQueries: {
        type: 'JSON',
        resolve: async(source, args, context, info) => {

          const type = info.schema.getType("Mdx");
          const resolver = type.getFields()["mdxAST"].resolve;
          const mdxAST = await resolver(source, args, context, {
            ...info,
            fieldName: "mdxAST"
          });

          function getAllJsx(x) {
            if(x.type === "jsx") {
              return [ x.value ];
            }
            else if(x.children && x.children.length) {
              return x.children.flatMap(getAllJsx);
            }
            return [];
          }

          const allQueries = getAllJsx(mdxAST)
                .flatMap(x=> {
                  // TODO this is brittle
                  //    - extract tag name and all props
                  //    - switch on tag name and use tag specific props->query logic
                  const result = x.match(/^<EmbeddedImage.*image="([^"]*)"[^>]*\/>/)
                  if(result && result[1]) {
                    return [{
                      type: "PhotoAsset",
                      query: {
                        filter: { title: { eq: result[1] } }
                      }
                    }]
                  }
                  else {
                    return [];
                  }
                } )

          return allQueries;
        }
      },
      related: {
        type: '[PhotoAsset]',
        resolve: async(source, args, context, info) => {
          const type = info.schema.getType("Mdx");
          const resolver = type.getFields()["relatedQueries"].resolve;
          const queries = await resolver(source, args, context, {
            ...info,
            fieldName: "relatedQueries"
          });

          const results = await Promise.all(queries.map(q => context.nodeModel.findOne(q)))
          return results;
        }
      }
    }
  }

  createResolvers(resolvers)
}
