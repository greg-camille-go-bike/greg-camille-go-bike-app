const { TourStatus, MediaType, TransportationMode, AssetScope, MediaLayout } = require("./schema/enums")
const { Collection, CollectionAsset, MediaLibrary, MediaAsset } = require("./schema/collection")
const { Tour } = require("./schema/tour")
const { Journal } = require("./schema/journal")
const { LocationRecording, LocationPoint, LocationTagged } = require("./schema/location")
const { Image, PhotoAsset } = require("./schema/image")
const { Video, VideoSource, VideoPreview, VideoData, VideoAsset } = require("./schema/video")
const { Page, Post, StaticPage } = require("./schema/page")

exports.createSchemaCustomization = ({ actions, schema }) => {
  const { createTypes, buildEnumType } = actions

  createTypes([
    schema.buildEnumType(TourStatus),
    schema.buildEnumType(MediaType),
    schema.buildEnumType(TransportationMode),
    schema.buildEnumType(AssetScope),
    schema.buildEnumType(MediaLayout),
    schema.buildInterfaceType(Collection),
    schema.buildObjectType(MediaLibrary),
    schema.buildInterfaceType(CollectionAsset),
    schema.buildInterfaceType(Page),
    schema.buildInterfaceType(Post),
    schema.buildObjectType(Tour),
    schema.buildObjectType(Journal),
    schema.buildObjectType(StaticPage)
  ])

  createTypes([
    schema.buildInterfaceType(LocationTagged),
    schema.buildObjectType(LocationPoint),
    schema.buildObjectType(LocationRecording)
  ])

  createTypes([
    schema.buildObjectType(Image),
    schema.buildObjectType(PhotoAsset)
  ])

  createTypes([
    schema.buildObjectType(Video),
    schema.buildObjectType(VideoSource),
    schema.buildObjectType(VideoPreview),
    schema.buildObjectType(VideoData),
    schema.buildObjectType(VideoAsset),
  ])

  createTypes([
    schema.buildInterfaceType(MediaAsset)
  ])
}
