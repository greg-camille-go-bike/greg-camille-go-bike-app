const { resolveSlug } = require("./admin")

const createUrlResolver = (prefix) => {
  return async (source, args, context, info) => {
    let slug = await resolveSlug(source, args,
                                   context, { fieldName: "slug" });

    if(slug.toLowerCase() === "home" || slug.toLowerCase() === "index" ) {
      slug = ""
    }
    else {
      slug = `${slug}/`
    }

    return `${prefix}${slug}`;
  }
}

const resolvePageBySlug = async (context, pageSlug) => {
  return context.nodeModel.findOne({
    type: "Page",
    query: {
      filter: { "pageSlug": { eq: pageSlug } }
    }
  })
}

const resolvePageParent = async (source, args, context, info) => {
  if(source.pageParent) {
    return await resolvePageBySlug(context, source.pageParent);
  }
  return null;
}

const resolvePageChildren = async (source, args, context, info) => {
    const { entries } = await context.nodeModel.findAll({
    type: "Page",
    query: {
      filter: { pageParent: { pageSlug: { eq: source.pageSlug } } },
      sort: {fields: ["sortDate"], order: ["ASC"]}
    }
  })
  return entries ? entries : [];
}

const resolvePageDescendants = async (source, args, context, info) => {
  // TODO
  return [];
}

const resolvePagePrevious = async (source, args, context, info) => {
  if(source.pageParent) {
    const { entries } = await context.nodeModel.findAll({
      type: "Page",
      query: {
        filter: { pageParent: { pageSlug: { eq: source.pageParent } } },
        sort: {fields: ["sortDate"], order: ["ASC"]}
      }
    })

    let previous=null
    let match=null
    entries.forEach((v) => {
      if(v.id === source.id){
        match=previous
      }
      else {
        previous=v
      }
    })
    return match;
  }
  return null;
}

const resolvePageNext = async (source, args, context, info) => {
  if(source.pageParent) {
    const { entries } = await context.nodeModel.findAll({
      type: "Page",
      query: {
        filter: { pageParent: { pageSlug: { eq: source.pageParent } } },
        sort: {fields: ["sortDate"], order: ["ASC"]}
      }
    })

    let matched=false
    let next=null
    entries.forEach((v) => {
      if(matched && !next){
        next = v
      }
      else if(v.id === source.id){
        matched = true
      }
    })
    return next
  }
  return null;
}

// Complete garbage, Gatsby is really stupid about sorting by dates...
const resolvePageSortDate = async (source, args, context, info) => {
  let sortDate = null;

  if(source.publishDate){
    return source.publishDate;
  }
  else if(source.endDate) {
    return source.endDate;
  }
  else if(source.startDate) {
    return source.startDate;
  }

  if(sortDate) {
    return sortDate.split("T")[0];
  }
  else {
    return null;
  }
}

const resolvePageLibrary = async (source, args, context, info) => {
  let libraryId = "default"
  if(source.library) {
    libraryId = source.library;
  }
  else {
    const pageParent = await resolvePageParent(source, args, context, info)
    if(pageParent) {
      return await resolvePageLibrary(pageParent, args, context, info)
    }
  }

  return await context.nodeModel.findOne({
    type: "MediaLibrary",
    query: {
      filter: { "libraryId": { eq: libraryId } }
    }
  })
}

const pageDefaultFields = {
    pageParent: {
      type: "Page",
      resolve: resolvePageParent
    },
    pageChildren: {
      type: "[Page!]",
      resolve: resolvePageChildren
    },
    pageDescendants: {
      type: "[Page!]"
    },
    pagePrevious: {
      type: "Page",
      resolve: resolvePagePrevious
    },
    pageNext: {
      type: "Page",
      resolve: resolvePageNext
    },
    sortDate: {
      type: "String",
      resolve: resolvePageSortDate
    },
    library: {
      type: "MediaLibrary",
      resolve: resolvePageLibrary
    }
}

exports.createUrlResolver = createUrlResolver;
exports.resolvePageParent = resolvePageParent;
exports.resolvePageChildren = resolvePageChildren;
exports.resolvePageDescendants = resolvePageDescendants;
exports.resolvePagePrevious = resolvePagePrevious;
exports.resolvePageNext = resolvePageNext;
exports.resolvePageSortDate = resolvePageSortDate;
exports.resolvePageLibrary = resolvePageLibrary;
exports.pageDefaultFields = pageDefaultFields;
