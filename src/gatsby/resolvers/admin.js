
const resolveSlug = async (source, args, context, info) => {
  // TODO improve me
  // filename probably needs to be url encoded?
  // any complexity with many nodes generated from a single file?
  let node = source
  while(node.internal.type !== "File" && node.parent) {
    node = context.nodeModel.getNodeById({id: node.parent})
  }

  if(node.internal.type === "File"){
    return node.name
  }
  else {
    throw new Error(`Failed to resolve slug for node ${source.id}`);
  }
}

const createEditLinkResolver = (collection) => {
  return async (source, args, context, info) => {
    const slug = await resolveSlug(source, args,
                                   context, { fieldName: "slug" });

    return `/admin/#/collections/${collection}/entries/${slug}`;
  }
}

exports.resolveSlug = resolveSlug;
exports.createEditLinkResolver = createEditLinkResolver;
