const scopeToPriority = (scope) => {
    if(scope && scope !== "UNKNOWN") {
      return ["HIDDEN","POST","COLLECTION","GLOBAL"].indexOf(scope);
    }
    else {
      return 1;
    }
}

const resolveMediaPriority = (source, args, context, info) => {
  return scopeToPriority(source.scope);
}

const resolveMediaLayout = (source, args, context, info) => {
  const ratio = source.source.width / source.source.height;
  if(ratio <= 1) {
    return "VERTICAL";
  }
  else if(ratio <= 1.5) {
    return "HORIZONTAL";
  }
  else if (ratio <= 2) {
    return "WIDE";
  }
  else {
    return "PANORAMA";
  }
}

const mediaDefaultFields = {
  scope: { type: "AssetScope!" },
  priority: {
    type: "Int!",
    resolve: resolveMediaPriority
  },
  layout: {
    type: "MediaLayout!",
    resolve: resolveMediaLayout
  }
}

exports.scopeToPriority = scopeToPriority;
exports.resolveMediaPriority = resolveMediaPriority;
exports.resolveMediaLayout = resolveMediaLayout;
exports.mediaDefaultFields = mediaDefaultFields;
