const express = require("express");
const { registerLocalFs } = require("netlify-cms-proxy-server/dist/middlewares");

exports.onCreateDevServer = async ({ app }) => {
  console.log("Enabling local assets CDN under /assets prefix")
  app.use("/images", express.static("../assets/images"))
  app.use("/images", express.static("../assets/stage/images"))

  app.use("/video", express.static("../assets/video"))
  app.use("/video", express.static("../assets/stage/video"))

  app.use("/source", express.static("../assets/source"))
  app.use("/source", express.static("../assets/stage/source"))

  console.log("Adding support for local cms editing")
  return await registerLocalFs(app)
}
