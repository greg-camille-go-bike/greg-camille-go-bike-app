const path = require("path")

const createJournalPages =  ({ graphql, actions }) => {
  const { createPage } = actions
  const journalsTemplate = path.resolve('src/templates/JournalPage.js')

  return graphql(`
    query {
      allTourJournal {
        nodes {
        id
        url
        }
      }
    }
  `).then(result => {
    if (result.errors) {
      throw result.errors
    }

    result.data.allTourJournal.nodes
      .forEach(journal => {
        createPage({
          path: `${journal.url}`,
          component: journalsTemplate,
          context: {
            id: journal.id
        },
      })
    })
  })

}

const createTourPages = ({graphql, actions}) => {
  const { createPage } = actions
  const toursTemplate = path.resolve('src/templates/TourPage.js')

  return graphql(`
    query {
      allTour {
        nodes {
          id
          url
      }
    }
  }
`).then(result => {
    if (result.errors) {
      throw result.errors
    }
    result.data.allTour.nodes
      .forEach(tour => {
        createPage({
          path: `${tour.url}`,
          component: toursTemplate,
          context: {
            id: tour.id
        },
      })
    })
  })
}

exports.createPages = (api) => {
  return createTourPages(api).then(() => createJournalPages(api) )
}
