import { graphql } from 'gatsby'

export const LocationData=graphql`
  fragment LocationData on LocationTagged {
    locationData {
      latitude
      longitude
      altitude
    }
  }
`

export const LocationRecordingSummary=graphql`
  fragment LocationRecordingSummary on LocationRecording {
    id
    gpsData(tolerance: 0.001)
    sequence
    date
    mode
  }
`

export const LocationRecordingDetailed=graphql`
  fragment LocationRecordingDetailed on LocationRecording {
    id
    gpsData(tolerance: 0.0001)
    sequence
    date
    mode
  }
`
