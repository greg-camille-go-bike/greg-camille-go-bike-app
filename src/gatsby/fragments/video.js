import { graphql } from 'gatsby'

export const VideoData=graphql`
  fragment VideoData on VideoAsset {
    id
    title
    description
    scope
    priority
    layout
    createDateTime
    videoData {
      sources {
        src
        type
      }
      preview {
        image {
        ...ImageData
        ...LocationData
        }
        sources {
          src
          type
        }
      }
    }
  }
`
