import { graphql } from 'gatsby'

export const ImageData=graphql`
  fragment ImageData on PhotoAsset {
    id
    title
    description
    scope
    priority
    layout
    createDateTime
    gatsbyImageData
  }
`
