import { graphql } from 'gatsby'

export const PageMetadata=graphql`
  fragment PageMetadata on Page {
    id
    pageType
    title
    url
    editUrl
    featureImage {
      gatsbyImageData
      description
    }
    ... on Post {
      displayDateLine
    }
  }
`

export const PageFull=graphql`
  fragment PageFull on Page {
    parent {
      ... on Mdx {
        slug
        body
      }
    }
  }
`

export const PagePreview=graphql`
  fragment PagePreview on Page {
    parent {
      ... on Mdx {
        excerpt(pruneLength: 400)
      }
    }
  }
`

export const PageSnippet=graphql`
  fragment PageSnippet on Page {
    parent {
      ... on Mdx {
        excerpt(pruneLength: 200)
      }
    }
  }
`

export const RelatedPage=graphql`
  fragment RelatedPage on Page {
    ...PageMetadata
    ...PageSnippet
  }
`

export const RelatedPages=graphql`
  fragment RelatedPages on Page {
    pagePrevious {
      ...RelatedPage
    }
    pageNext {
      ...RelatedPage
    }
    pageParent {
      ...RelatedPage
    }
  }
`

export const InlinePageChildren=graphql`
fragment InlinePageChildren on Page {
    pageChildren {
      ...PageMetadata
      ...PageFull
      ... on TourJournal {
        distanceTraveled
      }
    }
}
`
