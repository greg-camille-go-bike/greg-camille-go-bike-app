import { graphql } from 'gatsby'

export const MediaData=graphql`
  fragment MediaData on MediaAsset {
    id
    ... on VideoAsset {
      ...VideoData
      ...LocationData
    }
    ... on PhotoAsset {
      ...ImageData
      ...LocationData
    }
  }
`
