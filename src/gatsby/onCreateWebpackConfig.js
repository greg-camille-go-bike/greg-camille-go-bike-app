
const SSR_EXCLUSIONS = [ /node_modules\/leaflet/, /node_modules\\leaflet/ ];

exports.onCreateWebpackConfig = (api) => {
  const { stage, loaders, actions } = api
  if (stage === "build-html" || stage === "develop-html") {
    actions.setWebpackConfig({
      module: {
        rules: [{
          test: SSR_EXCLUSIONS,
          use: loaders.null()
        }]
      }
    });
  }
};
