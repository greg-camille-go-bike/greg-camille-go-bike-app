
const MediaLibrary = {
  name: "MediaLibrary",
  interfaces: ["Node"],
  fields: {
    id: {
      type: "ID!",
    },
    libraryId: {
      type: "String!"
    },
    title: {
      type: "String!"
    }
  }
}

const Collection = {
    name: "Collection",
    interfaces: ["Node"],
    fields: {
        id: {
            type: "ID!",
        },
        title: {
            type: "String!"
        }
    }
}

const CollectionAsset = {
    name: "CollectionAsset",
    interfaces: ["Node"],
    fields: {
      id: {
        type: "ID!",
      },
      library: {
        type: "MediaLibrary",
        extensions: {
          link: {
            by: "libraryId"
          }
        }
      },
      createDateTime: {
        type: "Date"
      },
      createLocalDate: {
        type: "Date"
      }
    }
}

const MediaAsset = {
  name: `MediaAsset`,
  interfaces: ["CollectionAsset","Node", "LocationTagged"],
  fields: {
    ...CollectionAsset.fields,
    scope: { type: "AssetScope!" },
    priority: { type: "Int!" },
    locationData: {
      type: "LocationPoint"
    }
  }
}

exports.Collection = Collection;
exports.CollectionAsset = CollectionAsset;
exports.MediaLibrary = MediaLibrary;
exports.MediaAsset = MediaAsset;
