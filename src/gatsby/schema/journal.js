const { formatDateLine } = require('../../util/resolvers')
const  { resolveSlug, createEditLinkResolver } = require("../resolvers/admin");
const { createUrlResolver, pageDefaultFields, resolvePageLibrary } = require("../resolvers/page")

function createFilterFunction(source) {
  return (asset) => {
    try {
      const assetDate = parseInt(asset.createLocalDate.substr(0,10).replaceAll("-",""));
      const startDate = parseInt(source.startDate.substr(0,10).replaceAll("-",""))
      const endDate = source.endDate ? parseInt(source.endDate.substr(0,10).replaceAll("-","")) : startDate;
      // TODO add more robust handling for various false values of endDate
      return assetDate >= startDate && assetDate <= endDate;
    }
    catch(e) {
      console.error("Failed due to error", e)
      return false;
    }
  }
}

const resolveLocationRecordings = async (source, args, context, info) => {
  const library = await resolvePageLibrary(source, args, context, info);

  const { entries } = await context.nodeModel.findAll({
    type: "LocationRecording",
    query: {
      filter: { "library": { libraryId: { eq: library.libraryId } } },
      sort: { fields: ["createDateTime"], order: ["ASC"] }
    }
  })

  return entries.filter(recording => {
    // TODO fix me (only filters the start date, not date range)
    return recording.createLocalDate.substr(0,10) === source.startDate.substr(0,10);
  })
}

exports.Journal = {
    name: "TourJournal",
    interfaces: ["Node","Post","Page"],
    fields: {
      collection: {
          type: "Tour!",
          extensions: {
            link: {}
          }
      },
      title: {
        type: "String!"
      },
      draft: {
        type: "Boolean!"
      },
      pageSlug: {
        type: "String!"
      },
      featureImage: {
        type: "PhotoAsset",
        extensions: {
          link: {
            by: "title"
          }
        }
      },
      startDate: {
        type: "Date!",
        extensions: {
          dateformat: {}
        }
      },
      endDate: {
        type: "Date",
        extensions: {
          dateformat: {}
        }
      },
      displayDateLine: {
        type: 'String!',
        resolve: async (source, args, context, info) => {
          return formatDateLine(source.startDate, source.endDate);
        }
      },
      url: {
        type: 'String!',
        resolve: createUrlResolver("/journals/")
      },
      editUrl: {
        type: 'String!',
        resolve: createEditLinkResolver("journals")
      },
      distanceTraveled: {
        type: 'JSON!',
        resolve: async (source, args, context, info) => {
          const results = {}
          const iter = await resolveLocationRecordings(source, args, context, info)
          iter.forEach((e,i) => {
            const mode = e.mode.toLowerCase()
            results[mode] = (results[mode] ?? 0) + e.distance
          })
          return results;
        }
      },
      locationRecordings: {
        type: '[LocationRecording]',
        resolve: resolveLocationRecordings
      },
      media: {
        type: '[MediaAsset!]',
        resolve: async (source, args, context, info) => {
          const library = await resolvePageLibrary(source, args, context, info);

          const { entries } = await context.nodeModel.findAll({
            type: "MediaAsset",
            query: {
              filter: { "library": { libraryId: { eq: library.libraryId } }, priority: { gte: 1 } },
              sort: { fields: ["createDateTime"], order: ["ASC"] }
            }
          })

          return entries.filter(createFilterFunction(source));
        }
      },
      ...pageDefaultFields
    }
}
