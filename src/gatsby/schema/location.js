const fs = require('fs');
const simplify = require('@turf/simplify')
const bbox = require('@turf/bbox').default
const length = require('@turf/length')
const axios = require('axios');

const togeojson = require('@mapbox/togeojson');
const DOMParser = require('xmldom').DOMParser;

const LocationPoint = {
  name: "LocationPoint",
  fields: {
    date:       { type: "Date",
                    extensions: {
                    dateformat:{}
                    }
                },
    localDate:  { type: "Date",
                    extensions: {
                    dateformat: {}
                    }
                },
    latitude:   { type:"Float" },
    longitude:  { type:"Float" },
    altitude:   { type:"Float" },
    hasLocation: {
      type:"Boolean!",
      resolve: (source) => {
        return !! (source.rawLocation)
      }
    }
  }
}

const LocationTagged = {
  name: "LocationTagged",
  fields: {
    locationData: {
      type: "LocationPoint"
    }
  }
}

const resolveGeoJson = async (source, context, options={}) => {
  const { tolerance=0.001 } = options;
  const fileNode = await context.nodeModel.findOne({
    type: "File",
    query: {
      // TODO figure out a cleaner pattern
      filter: {relativePath: {eq: `location-data/${source.fileName}` }}
    }
  })

  const data = await fs.promises.readFile(fileNode.absolutePath, 'utf8');
  const extension = fileNode.absolutePath.split(".").slice(-1)[0]

  if(extension === "gpx") {
    var gpx = new DOMParser().parseFromString(data);
    var asJson = togeojson.gpx(gpx);
    return simplify(asJson, {tolerance: tolerance, highQuality: true});
  }
  else if(extension === "kml") {
    var kml = new DOMParser().parseFromString(data);
    var asJson = togeojson.kml(kml);
    return simplify(asJson, {tolerance: tolerance, highQuality: true});
  }
  else {
    const asJson = JSON.parse(data);
    return simplify(asJson, {tolerance: tolerance, highQuality: true});
  }
}

const reverseGeoCode = async (latitude, longitude) => {
  if(typeof latitude === "number" && typeof longitude === "number") {
    try {
      const url = `https://nominatim.openstreetmap.org/reverse?lat=${latitude}&lon=${longitude}&format=jsonv2&zoom=14`

      const response = await axios.get(url)

      // TODO figure out how to prioritize village, town, municipality, city, etc and format appropriately
      // state does seem to be a consistently good option (at least for US and Canada)
      // alternatively, maybe just use display_name and strip zip code?

      return response.data;
//      return `${response?.data?.name}, ${response?.data?.address?.state}`;
    }
    catch(e) {
      console.log("Failed with error", e)
      return ""
    }
  }
  else {
    return "";
  }
}

const formatLocation = async(coordinate) => {
  const [ longitude, latitude ] = coordinate;
  const altitude = coordinate && coordinate.length > 2 ? coordinate[2] : null
  //const label = await reverseGeoCode(latitude, longitude)

  return {
//    label,
    latitude,
    longitude,
    altitude
  }
}

const createSummary = async (geojson, args) => {
  const { tolerance=0.001, units='miles' } = args
  const feature = geojson?.features[0].geometry

    // TODO better handling for potential range of forms for the features
    // ex: MultiLineString may actually have multiplelines or even if the geojson has multiple features...
    const coordinates = feature.type === "MultiLineString" ? feature.coordinates[0] : feature.coordinates

    const bounds = bbox(geojson)

    let toLength = length.default? length.default : length;
    const distance = toLength(geojson, { units: units });

    const start = await formatLocation(coordinates[0]);
    const end = await formatLocation(coordinates[coordinates.length - 1]);

    return {
      start: start,
      end: end,
      bounds: {
        latitude: {
          min: bounds[1],
          max: bounds[3]
        },
        longitude: {
          min: bounds[0],
          max: bounds[2]
        }
      },
      distance: {
        value: distance,
        units: units
      }
    }
}

const LocationRecording = {
  name: "LocationRecording",
  interfaces: [ "Node","CollectionAsset" ],
  fields: {
    collection:       { type: "Collection!",
                        extensions: { link: {} } },
    library: {
      type: "MediaLibrary",
      extensions: {
        link: {
          by: "libraryId"
        }
      }
    },
    title:            { type: "String!"},
    mode:             { type: "TransportationMode!"},
    begin:            { type: "String" },
    end:              { type: "String" },
    date:             { type: "Date" },
    sequence:         { type: "Int" },
    distance:         { type: "Float" },
    filename:         { type: "File",
                        extensions: { fileByRelativePath: {} } },
    createDateTime:   { type: "Date" },
    createLocalDate:  { type: "Date" },
    geojson: {
      type: 'JSON',
      args: {
        tolerance: 'Float'
      },
      resolve: async (source, args, context, info) => {
        return resolveGeoJson(source, context, args)
      }
    },
    summary: {
        type: 'JSON',
        args: {
          tolerance: 'Float',
          units: 'String'
        },
        resolve: async (source, args, context, info) => {
          const { tolerance=0.001, units='miles' } = args

          // TODO check the json file first, if not found compute it and then update the source
          const geojson = await resolveGeoJson(source, context, { tolerance })
          return createSummary(geojson, args);
      }
    },
    gpsData: {
      type: 'JSON',
      args: {
        tolerance: 'Float',
        // TODO make an enum
        units: 'String'
      },
      resolve: async (source, args, context, info) => {
        const { tolerance=0.001, units='miles' } = args
        const geojson = await resolveGeoJson(source, context, { tolerance })
        const summary = await createSummary(geojson, args);

        return {
        bbox: {
          minX: summary.bounds.longitude.min,
          minY:summary.bounds.latitude.min,
          maxX: summary.bounds.longitude.max,
          maxY: summary.bounds.latitude.max
        },
        length: summary.distance.value,
        coordinates: geojson
        };
      }
    }
  }
}

exports.LocationPoint = LocationPoint
exports.LocationTagged = LocationTagged
exports.LocationRecording = LocationRecording
