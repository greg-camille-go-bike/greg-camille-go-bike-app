const { mediaDefaultFields } = require("../resolvers/media")

const Video = {
  name: "Video",
  fields: {
    fileName:   { type: "String!" },
    fileSize:   { type: "Int!" },
    width:      { type: "Int!" },
    height:     { type: "Int!" }
  }
}

const prefix=`${process.env.GATSBY_ASSETS_PREFIX_URL||''}/source/video`
const previewPrefix=`${process.env.GATSBY_ASSETS_PREFIX_URL||''}/video`

const VideoSource = {
  name: "VideoSource",
  fields: {
    src:  { type: "String!" },
    type: { type: "String!" }
  }
}

const VideoPreview = {
  name: "VideoPreview",
  fields: {
    image: {
      type: "PhotoAsset",
      extensions: {
        link: {
          by: "title"
        }
      }
    },
    sources: {
      type: "[VideoSource!]"
    }
  }
}


const VideoData = {
  name: "VideoData",
  fields: {
    preview: {
      type: "VideoPreview"
    },
    sources: {
      type: "[VideoSource!]"
    }
  }
}

const VideoAsset = {
  name: "VideoAsset",
  interfaces: [ "Node", "CollectionAsset", "LocationTagged", "MediaAsset" ],
  fields: {
    collection:       { type: "Collection!",
                        extensions: { link: {} } },
    library: {
      type: "MediaLibrary",
      extensions: {
        link: {
          by: "libraryId"
        }
      }
    },
    mediaType:        { type: "MediaType!" },
    title:            { type: "String!" },
    source:           { type: "Video!" },
    locationData:     { type: "LocationPoint!" },
    description:      { type: "String!" },
    ...mediaDefaultFields,
    createDateTime:   { type: "Date" },
    createLocalDate:  { type: "Date" },
    videoData: {
      type: "VideoData",
      resolve: async (source, args, context, info) => {
        const previewSources = source?.preview?.videos?.map(x=> {
          return {
            src: `${previewPrefix}/${x.fileName}`,
            type: x.mediaType
          }
        }) ?? []

        return {
          preview: {
            image: source?.preview?.image,
            sources: previewSources
          },
          sources: [
            {
              src: `${prefix}/${source.source.fileName}`,
              type: `video/mp4`
            }
          ]
        }
      }
    }
  }
}

exports.Video = Video
exports.VideoSource = VideoSource
exports.VideoPreview = VideoPreview
exports.VideoData = VideoData
exports.VideoAsset = VideoAsset
