const { formatDateLine } = require('../../util/resolvers')
const  { resolveSlug, createEditLinkResolver } = require("../resolvers/admin");
const { createUrlResolver, pageDefaultFields, resolvePageLibrary } = require("../resolvers/page")

exports.Tour = {
  name: "Tour",
  interfaces: ["Node","Collection","Post","Page"],
  fields: {
    title: {
      type: "String!"
    },
    pageSlug: {
      type: "String!"
    },
    displayDateLine: {
        type: 'String!',
        resolve: async (source, args, context, info) => {
          return ''; // TODO resolve start/end date based on first/last journal
//          return formatDateLine(source.startDate, source.endDate);
        }
    },
    url: {
      type: 'String!',
      resolve: createUrlResolver("/tours/")
    },
    editUrl: {
      type: 'String!',
      resolve: createEditLinkResolver("tours")
    },
    featureImage: {
      type: "PhotoAsset",
      extensions: {
        link: {
          by: "title"
        }
      }
    },
    status: {
      type: "TourStatus"
    },
    media: {
      type: '[MediaAsset!]',
      resolve: async (source, args, context, info) => {
        const library = await resolvePageLibrary(source, args, context, info);

        const { entries } = await context.nodeModel.findAll({
          type: "MediaAsset",
          query: {
            sort: { fields: ["createDateTime"], order: ["ASC"] },
            filter: {library: { libraryId: { eq: library.libraryId }}, priority: {gte: 2 }}
          }
        })
        return entries;
      }
    },
    locationRecordings: {
      type: '[LocationRecording]',
      resolve: async (source, args, context, info) => {
        const library = await resolvePageLibrary(source, args, context, info);

        const { entries } = await context.nodeModel.findAll({
          type: "LocationRecording",
          query: {
            sort: { fields: ["createDateTime"], order: ["ASC"] },
            filter: {library: { libraryId: { eq: library.libraryId } } }
          }
        })
        return entries;
      }
    },
    ...pageDefaultFields
  }
}
