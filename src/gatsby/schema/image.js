const fs = require('fs');
const { mediaDefaultFields } = require("../resolvers/media")
const { IMAGE_BASEDIR, IMAGE_STAGE_BASEDIR, TYPES, DEFAULT_IMAGE_SCALES  } = require("../../scripts/image/constants")

function findTransform(transforms, config, width, mediaType) {
  const versions = transforms[config][mediaType]
  for (let i in versions) {
    const next = versions[i];
    // find something reasonably close to the expected size (note this doesn't yet work for WEBP)
    if(Math.abs(next.width - width) < 2) {
      return next;
    }
  }
  return null;
}

async function loadPlaceholderContent(transform) {
  const path = `${IMAGE_BASEDIR}/${transform.fileName}`;
  const altPath = `${IMAGE_STAGE_BASEDIR}/${transform.fileName}`;

  return fs.promises.readFile(path)
    .catch(x => fs.promises.readFile(altPath))
    .then(buffer=> base64Encode(buffer, transform.mediaType))
}

function base64Encode(buffer, mediaType) {
  const asBase64 = buffer.toString("base64");
  return `data:${mediaType};base64,${asBase64}`
}

// TODO extract to a common utility location
async function formatGatsbyImageData(node) {
  const prefix=`${process.env.GATSBY_ASSETS_PREFIX_URL||''}/images`
  const placeholder = node.transforms.blur[TYPES.JPEG.mediaType].filter(x => x.width <= 50)[0]
  const placeholderContent = await loadPlaceholderContent(placeholder);

  // in the future could come from params passed to the resolver
  const targetWidths = DEFAULT_IMAGE_SCALES.map(x => x*node.source.width)
  const transformConfig="noop"
  const fallbacks = targetWidths.map(x => findTransform(node.transforms, transformConfig, x, TYPES.JPEG.mediaType)).filter(x=> !!x);
  const bigFallback = fallbacks[fallbacks.length - 1]
  const primaries = targetWidths.map(x => findTransform(node.transforms, transformConfig, x, TYPES.WEBP.mediaType)).filter(x=> !!x);
  const bigPrimary = primaries[primaries.length - 1]

  const avifSources = targetWidths.map(x => findTransform(node.transforms, transformConfig, x, TYPES.AVIF.mediaType)).filter(x=> !!x);
  const avifBig = avifSources[avifSources.length - 1]

  // add guard to check all required else return null
  if(/*placeholderContent &&*/ fallbacks.length > 1 && primaries.length > 1) {
    return {
      "layout": "constrained",
      "placeholder": {
        "fallback": placeholderContent
      },
      images: {
        "fallback": {
          "src": `${prefix}/${bigFallback.fileName}`,
          "srcSet": fallbacks.map(x => `${prefix}/${x.fileName} ${x.width}w` ).join(",\n"),
          "sizes": `(min-width: ${bigFallback.width}px) ${bigFallback.width}px, 100vw`
        },
        "sources": [
          {
            "srcSet": avifSources.map(x => `${prefix}/${x.fileName} ${x.width}w` ).join(",\n"),
            "type": "image/avif",
            "sizes": `(min-width: ${avifBig.width}px) ${avifBig.width}px, 100vw`
          },
          {
            "srcSet": primaries.map(x => `${prefix}/${x.fileName} ${x.width}w` ).join(",\n"),
            "type": "image/webp",
            "sizes": `(min-width: ${bigPrimary.width}px) ${bigPrimary.width}px, 100vw`
          }
        ]
      },
      "width": node.source.width,
      "height": node.source.height
    }
  }
  return null;
}

const Image = {
  name: "Image",
  fields: {
    fileName:   { type: "String" },
    fileSize:   { type: "Int" },
    width:      { type: "Int" },
    height:     { type: "Int" },
    format:     { type: "String" },
    content:    { type: "String" }
  }
}

const PhotoAsset = {
  name: "PhotoAsset",
  interfaces: [ "Node", "CollectionAsset", "LocationTagged", "MediaAsset" ],
  fields: {
    collection:       { type: "Collection!",
                        extensions: { link: {} } },
    library: {
      type: "MediaLibrary",
      extensions: {
        link: {
          by: "libraryId"
        }
      }
    },
    mediaType:        { type: "MediaType!" },
    title:            { type: "String!" },
    source:           { type: "Image!" },
    locationData:     { type: "LocationPoint!" },
    description:      { type: "String!" },
    ...mediaDefaultFields,
    createDateTime:   { type: "Date" },
    createLocalDate:  { type: "Date" },
    transforms:       { type: "[Image!]" },
    gatsbyImageData: {
      type: 'JSON',
      resolve: async (source, args, context, info) => {
        // first check if it's already been prepared and uploaded to S3
        // --OR-- has a local copy
        const cached = await formatGatsbyImageData(source)
        if(cached){
          return cached;
        }

        // Else delegate to gatsby-transformer-sharp
        const fileNode = await context.nodeModel.findOne({
        type: "File",
        query: {
            filter: {relativePath: {eq: source.source.fileName}}
        }
        })

        if(null != fileNode && fileNode.children && fileNode.children.length > 0) {
        const imageSharpNode = await context.nodeModel.getNodeById({
            id: fileNode.children[0],
            type: "ImageSharp"
        })

        const type = info.schema.getType("ImageSharp");
        const resolver = type.getFields()["gatsbyImageData"].resolve;
        return await resolver(imageSharpNode, args, context, {
            fieldName: "gatsbyImageData"
        });
        }
        return null;
      }
    }
  }
}

exports.Image = Image;
exports.PhotoAsset = PhotoAsset;
exports.formatGatsbyImageData = formatGatsbyImageData;
