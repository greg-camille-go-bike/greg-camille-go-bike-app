exports.TourStatus = {
    name: 'TourStatus',
    values: {
        PLANNING: { value: 'PLANNING' },
        ACTIVE: { value: 'ACTIVE' },
        COMPLETE: { value: 'COMPLETE' }
    }
}

exports.MediaType = {
    name: 'MediaType',
    values: {
        IMAGE: { value: 'IMAGE' },
        VIDEO: { value: 'VIDEO' }
    }
}

exports.TransportationMode = {
    name: 'TransportationMode',
    values: {
        AUTOMOBILE: { value: 'AUTOMOBILE' },
        BICYCLE: { value: 'BICYCLE' },
        BOAT: { value: 'BOAT' },
        TRAIN: { value: 'TRAIN' }
    }
}

exports.AssetScope = {
    name: 'AssetScope',
    values: {
        GLOBAL: { value: 'GLOBAL' },
        COLLECTION: { value: 'COLLECTION' },
        POST: { value: 'POST' },
        UNKNOWN: { value: 'UNKNOWN' },
        HIDDEN: { value: 'HIDDEN' }
    }
}

exports.MediaLayout = {
    name: 'MediaLayout',
    values: {
        HORIZONTAL: { value: 'HORIZONTAL' },
        VERTICAL: { value: 'VERTICAL' },
        WIDE: { value: 'WIDE' },
        PANORAMA: { value: 'PANORAMA' }
    }
}
