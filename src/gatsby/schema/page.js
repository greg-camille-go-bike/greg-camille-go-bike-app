const { resolveSlug, createEditLinkResolver } = require("../resolvers/admin");
const { createUrlResolver, pageDefaultFields } = require("../resolvers/page")
const { findIndex } = require("lodash")

const Page = {
  name: "Page",
  interfaces: ["Node"],
  fields: {
    id: {
      type: "ID!",
    },
    pageSlug: {
      type: "String!"
    },
    title: {
      type: "String!"
    },
    featureImage: {
      type: "PhotoAsset"
    },
    url: {
      type: "String!"
    },
    editUrl: {
      type: "String!"
    },
    sortDate: {
      type: "String"
    },
    pageType: {
      type: "String"
    },
    pageParent: {
      type: "Page"
    },
    pageChildren: {
      type: "[Page!]"
    },
    pageDescendants: {
      type: "[Page!]"
    },
    pagePrevious: {
      type: "Page"
    },
    pageNext: {
      type: "Page"
    },
    library: {
      type: "MediaLibrary"
    }
  }
}

const Post = {
  name: "Post",
  interfaces: ["Page", "Node"],
  fields: {
    ...Page.fields,
    displayDateLine: {
      type: "String!"
    }
  }
}

const StaticPage = {
  name: "StaticPage",
  interfaces: ["Node","Page"],
  fields: {
    title: {
      type: "String!"
    },
    pageSlug: {
      type: "String!"
    },
    featureImage: {
      type: "PhotoAsset",
      extensions: {
        link: {
          by: "title"
        }
      }
    },
    url: {
      type: 'String!',
      resolve: createUrlResolver("/")
    },
    editUrl: {
      type: 'String!',
      resolve: createEditLinkResolver("pages")
    },
    ...pageDefaultFields
  }
}

exports.Page = Page;
exports.Post = Post;
exports.StaticPage = StaticPage;
