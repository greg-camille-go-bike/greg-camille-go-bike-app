# Greg Camille Go Bike

This repository contains the source code for Greg & Camille's personal blog where they explore places on bike. Check out the live website [here](https://www.gregcamillego.bike/).

## Development

1. One time setup of the repository
```sh
git clone https://gitlab.com/greg-camille-go-bike/greg-camille-go-bike-app.git
make init
```

2. Start a local development server
```sh
make develop
```
