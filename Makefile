##
# Greg Camille Go Bike
#
# @file
# @version 0.1

.DEFAULT_GOAL := help

submodules:
	@git submodule update --init --recursive
.PHONY: submodules

init: submodules
	@echo "Initialize project"
	@npm install
	@echo "End of initialization"
.PHONY: init

assets-upload:
	@echo "Not yet implemented"
	@false
.PHONY: assets-upload

assets-restore:
	@echo "Not yet implemented"
	@false
.PHONY: assets-restore

clean:
	@npm run clean
.PHONY: clean

develop:
	@npm run develop
.PHONY: develop

develop-lan:
	@echo "Starting Gatsby development instance available outside localhost"
	@npm run develop-lan
.PHONY: develop-lan

build:
	@npm run build
.PHONY: build

preview: build
	@npm run serve
.PHONY: preview

deploy-draft:
	@aws s3 sync public s3://greg-camille-go-bike-draft

deploy-main:
	@aws s3 sync public s3://greg-camille-go-bike-main

help: ## Show this help message
	@echo ''
	@echo 'Usage:'
	@echo '  make <target>'
	@echo ''
	@echo 'Targets:'
	@echo '  help           Show this help message'
	@echo '  init           Initialize the project by downloading all required dependencies'
	@echo '  assets-upload  Upload new local assets to object storage'
	@echo '  assets-restore Download assets from object storage not found locally'
	@echo '  build		    Build a release version of the blog'
	@echo '  clean          Clean the project of all intermediate, temporary and build artifacts'
	@echo '  deploy-draft   Deploy the blog to the draft bucket'
	@echo '  deploy-main    Deploy the blog to the main bucket'
	@echo '  develop        Start a development instance of the blog'
	@echo '  preview		Build a release version of the blog and preview it locally'

.PHONY: help
# end
