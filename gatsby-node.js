const { createSchemaCustomization } = require("./src/gatsby/createSchemaCustomization");
const { onCreateNode } = require("./src/gatsby/onCreateNode")
const { createResolvers } = require("./src/gatsby/createResolvers")
const { createPages } = require("./src/gatsby/createPages")
const { onCreateDevServer } = require("./src/gatsby/onCreateDevServer")
const { onCreateWebpackConfig } = require("./src/gatsby/onCreateWebpackConfig")

exports.createSchemaCustomization = createSchemaCustomization;
exports.onCreateNode = onCreateNode
exports.createResolvers = createResolvers
exports.createPages = createPages
exports.onCreateDevServer = onCreateDevServer
exports.onCreateWebpackConfig = onCreateWebpackConfig
